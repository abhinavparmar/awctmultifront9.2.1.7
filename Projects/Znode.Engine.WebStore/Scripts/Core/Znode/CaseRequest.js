var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var CaseRequest = /** @class */ (function (_super) {
    __extends(CaseRequest, _super);
    function CaseRequest() {
        return _super.call(this) || this;
    }
    CaseRequest.prototype.Init = function () {
        CaseRequest.prototype.ValidationForContactUsForm();
        CaseRequest.prototype.ValidationForCustomerFeedbackForm();
        //Nivi Code--Start//
        CaseRequest.prototype.ValidationForModelSearchForm();
        CaseRequest.prototype.ValidationForNewCustomerForm();
        CaseRequest.prototype.ValidationForPreviewShowForm();
        CaseRequest.prototype.ValidationForQuoteLookupForm();
        CaseRequest.prototype.ValidationForBecomeAContributerForm();
        //End
    };
    //Set validation for inputs
    CaseRequest.prototype.ValidationForContactUsForm = function () {
        $("#contact-us").on("click", function () {
            var flag = true;
            //Set required field for first name
            var firstName = $("#valFirstName").val();
            if (firstName.length < 1) {
                $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredFirstName"));
                flag = false;
            }
            else {
                $("#valFirstNameErr").html("");
            }
            //Set required field for last name
            var lastName = $("#valLastName").val();
            if (lastName.length < 1) {
                $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredLastName"));
                flag = false;
            }
            else {
                $("#valLastNameErr").html("");
            }
            //Nivi Code-Start
            var CompanyName = $("#valCompanyName").val();
            if (CompanyName.length < 1) {
                $("#valCompanyNameErr").html("Company Name is required.");
                flag = false;
            }
            else {
                $("#valCompanyNameErr").html("");
            }
            //Nivi Code-End
            //Set required field for comment
            var comment = $("#valComment").val();
            if (comment.length < 1) {
                $("#valCommentErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredComment"));
                flag = false;
            }
            else {
                $("#valCommentErr").html("");
            }
            //Validate phone number
            var phoneNum = $("#valPhoneNum").val();
            if (phoneNum.length < 1) {
                $("#valPhoneNumErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredPhoneNumber"));
                flag = false;
            }
            //Validate email address
            var email = $("#valEmail").val();
            if (email.length < 1) {
                $("#valEmailErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"));
                flag = false;
            }
            else {
                $("#valEmailErr").html("");
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    $("#valEmailErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                    flag = false;
                }
            }
            return flag;
        });
    };
    //Set validation for Customer Feedback Form
    CaseRequest.prototype.ValidationForCustomerFeedbackForm = function () {
        $("#customer-feedback").on("click", function () {
            var flag = true;
            var FirstName = $("#FirstName").val();
            if (FirstName.length < 1) {
                $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredFirstName"));
                flag = false;
            }
            var LastName = $("#LastName").val();
            if (LastName.length < 1) {
                $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredLastName"));
                flag = false;
            }
            //Validate email address
            var email = $("#valEmailAddress").val();
            if (email.length < 1) {
                $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredEmailId"));
                flag = false;
            }
            else {
                $("#valEmailAddressErr").html("");
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                    flag = false;
                }
            }
            return flag;
        });
    };
    //Nivi Code--Start
    CaseRequest.prototype.ValidationForNewCustomerForm = function () {
        var flag = true;
        var FirstName = $("#valFirstName").val();
        if (FirstName.length < 1) {
            $("#valFirstNameErr").html("First Name is required.");
            flag = false;
        }
        else {
            $("#valFirstNameErr").html("");
        }
        var LastName = $("#valLastName").val();
        if (LastName.length < 1) {
            $("#valLastNameErr").html("Last Name is required.");
            flag = false;
        }
        else {
            $("#valLastNameErr").html("");
        }
        var StudioName = $("#valStudioName").val();
        if (StudioName.length < 1) {
            $("#valStudioNameErr").html("Studio Name is required.");
            flag = false;
        }
        else {
            $("#valStudioNameErr").html("");
        }
        var email = $("#valEmailAddress").val();
        if (email.length < 1) {
            $("#valEmailAddressErr").html("Email Address is required.");
            flag = false;
        }
        else {
            $("#valEmailAddressErr").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        var PhoneNumber = $("#valPhoneNumber").val();
        if (PhoneNumber.length < 1) {
            $("#valPhoneNumberErr").html("Phone Number is required.");
            flag = false;
        }
        else {
            $("#valPhoneNumberErr").html("");
        }
        var CellPhoneNumber = $("#valCellPhoneNumber").val();
        if (CellPhoneNumber.length < 1) {
            $("#valCellPhoneNumberErr").html("Cell Phone Number is required.");
            flag = false;
        }
        else {
            $("#valCellPhoneNumberErr").html("");
        }
        var MailingAddress = $("#valMailingAddress").val();
        if (MailingAddress.length < 1) {
            $("#valMailingAddressErr").html("Mailing Address is required.");
            flag = false;
        }
        else {
            $("#valMailingAddressErr").html("");
        }
        var DeliveryAddress = $("#valDeliveryAddress").val();
        if (DeliveryAddress.length < 1) {
            $("#valDeliveryAddressErr").html("Delivery Address is required.");
            flag = false;
        }
        else {
            $("#valDeliveryAddressErr").html("");
        }
        //var WebsiteAddress: string = $("#valWebsiteAddress").val();
        //if (WebsiteAddress.length < 1) {
        //    $("#valWebsiteAddressErr").html("Website Address is required.");
        //    flag = false;
        //}
        //else {
        //    $("#valWebsiteAddressErr").html("");
        //}
        var StudioAffiliationFile = $("#valStudioAffiliationFile").val();
        if (StudioAffiliationFile.length < 1) {
            $("#valStudioAffiliationFileErr").html("Proof of Studio Affiliation is required.");
            flag = false;
        }
        else {
            $("#valStudioAffiliationFileErr").html("");
        }
        return flag;
    };
    CaseRequest.prototype.ValidationForModelSearchForm = function () {
        var cartCount = 0;
        var flag = true;
        var modelName = $("#valModelName").val();
        if (modelName.length < 1) {
            $("#valModelNameErr").html("Model Name is required.");
            flag = false;
        }
        else {
            $("#valModelNameErr").html("");
        }
        var ContactName = $("#valContactName").val();
        if (ContactName.length < 1) {
            $("#valContactNameErr").html("Contact Name is required.");
            flag = false;
        }
        else {
            $("#valContactNameErr").html("");
        }
        var DayPhone = $("#valDayPhone").val();
        if (DayPhone.length < 1) {
            $("#valDayPhoneErr").html("Day Phone is required.");
            flag = false;
        }
        else {
            $("#valDayPhoneErr").html("");
        }
        var email = $("#valEmailAddress").val();
        if (email.length < 1) {
            $("#valEmailAddressErr").html("Email Address is required.");
            flag = false;
        }
        else {
            $("#valEmailAddressErr").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        var ArmLength = $("#valArmLength").val();
        if (ArmLength.length < 1) {
            $("#valArmLengthErr").html("Arm Length is required.");
            flag = false;
        }
        else {
            $("#valArmLengthErr").html("");
        }
        var ChestMeasurement = $("#valChestMeasurement").val();
        if (ChestMeasurement.length < 1) {
            $("#valChestMeasurementErr").html("Chest Measurement is required.");
            flag = false;
        }
        else {
            $("#valChestMeasurementErr").html("");
        }
        var GirthMeasurement = $("#valGirthMeasurement").val();
        if (GirthMeasurement.length < 1) {
            $("#valGirthMeasurementErr").html("Girth Measurement is required.");
            flag = false;
        }
        else {
            $("#valGirthMeasurementErr").html("");
        }
        var HipMeasurement = $("#valHipMeasurement").val();
        if (HipMeasurement.length < 1) {
            $("#valHipMeasurementErr").html("Hip Measurement is required.");
            flag = false;
        }
        else {
            $("#valHipMeasurementErr").html("");
        }
        var InseamMeasurement = $("#valInseamMeasurement").val();
        if (InseamMeasurement.length < 1) {
            $("#valInseamMeasurementErr").html("Inseam Measurement is required.");
            flag = false;
        }
        else {
            $("#valInseamMeasurementErr").html("");
        }
        var WaistMeasurement = $("#valWaistMeasurement").val();
        if (WaistMeasurement.length < 1) {
            $("#valWaistMeasurementErr").html("Waist Measurement is required.");
            flag = false;
        }
        else {
            $("#valWaistMeasurementErr").html("");
        }
        //var WaistMeasurement: string = $("#valWaistMeasurement").val();
        //if (WaistMeasurement.length < 1) {
        //    $("#valWaistMeasurementErr").html("is required.");
        //    flag = false;
        //}
        //else {
        //    $("#valWaistMeasurementErr").html("");
        //}
        var Height = $("#valHeight").val();
        if (Height.length < 1) {
            $("#valHeightErr").html("Height is required.");
            flag = false;
        }
        else {
            $("#valHeightErr").html("");
        }
        var HeadShotPhotoUpload = $("#valHeadShotPhotoUpload").val();
        if (HeadShotPhotoUpload.length < 1) {
            $("#valHeadShotPhotoUploadErr").html("Head Shot Photo is required.");
            flag = false;
        }
        else {
            $("#valHeadShotPhotoUploadErr").html("");
        }
        var FullLengthPhotoUpload = $("#valFullLengthPhotoUpload").val();
        if (FullLengthPhotoUpload.length < 1) {
            $("#valFullLengthPhotoUploadErr").html("Full Length Upload is required.");
            flag = false;
        }
        else {
            $("#valFullLengthPhotoUploadErr").html("");
        }
        return flag;
    };
    CaseRequest.prototype.ValidationForPreviewShowForm = function () {
        var flag = true;
        var FirstName = $("#FirstName").val().trim();
        if (FirstName.length < 1) {
            $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredFirstName"));
            flag = false;
        }
        else {
            $("#valFirstNameErr").html("");
        }
        var LastName = $("#LastName").val().trim();
        if (LastName.length < 1) {
            $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredLastName"));
            flag = false;
        }
        else {
            $("#valLastNameErr").html("");
        }
        var StudioName = $("#StudioName").val().trim();
        if (StudioName.length < 1) {
            $("#valStudioNameErr").html("Studio Name is required.");
            flag = false;
        }
        else {
            $("#valStudioNameErr").html("");
        }
        var Address = $("#valAddress").val().trim();
        if (Address.length < 1) {
            $("#valAddressErr").html("Address is required.");
            flag = false;
        }
        else {
            $("#valAddressErr").html("");
        }
        var City = $("#valCity").val().trim();
        if (City.length < 1) {
            $("#valCityErr").html("City is required.");
            flag = false;
        }
        else {
            $("#valCityErr").html("");
        }
        var State = $("#valState").val().trim();
        if (State.length < 1) {
            $("#valStateErr").html("State is required.");
            flag = false;
        }
        else {
            $("#valStateErr").html("");
        }
        var ZipCode = $("#valZipCode").val().trim();
        if (ZipCode.length < 1) {
            $("#valZipCodeErr").html("Zip Code is required.");
            flag = false;
        }
        else {
            $("#valZipCodeErr").html("");
        }
        var PhoneNumber = $("#valPhoneNumber").val().trim();
        if (PhoneNumber.length < 1) {
            $("#valPhoneNumberErr").html("Phone Number is required.");
            flag = false;
        }
        else {
            $("#valPhoneNumberErr").html("");
        }
        var email = $("#valEmailAddress").val();
        if (email.length < 1) {
            $("#valEmailAddressErr").html("Email Address is required.");
            flag = false;
        }
        else {
            $("#valEmailAddressErr").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        var NoOfTeacher = $("#valNoOfTeacher").val().trim();
        if (NoOfTeacher.length < 1) {
            $("#valNoOfTeacherErr").html("Number of teachers is required.");
            flag = false;
        }
        else {
            $("#valNoOfTeacherErr").html("");
        }
        return flag;
    };
    CaseRequest.prototype.ValidationForBecomeAContributerForm = function () {
        var flag = true;
        var FirstName = $("#valFirstName").val().trim();
        if (FirstName.length < 1) {
            $("#valFirstNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredFirstName"));
            flag = false;
        }
        else {
            $("#valFirstNameErr").html("");
        }
        var LastName = $("#valLastName").val().trim();
        if (LastName.length < 1) {
            $("#valLastNameErr").html(ZnodeBase.prototype.getResourceByKeyName("RequiredLastName"));
            flag = false;
        }
        else {
            $("#valLastNameErr").html("");
        }
        var Address = $("#valAddress").val().trim();
        if (Address.length < 1) {
            $("#valAddressErr").html("Address is required.");
            flag = false;
        }
        else {
            $("#valAddressErr").html("");
        }
        var City = $("#valCity").val().trim();
        if (City.length < 1) {
            $("#valCityErr").html("City is required.");
            flag = false;
        }
        else {
            $("#valCityErr").html("");
        }
        var State = $("#valStateProvince").val().trim();
        if (State.length < 1) {
            $("#valStateProvinceErr").html("State is required.");
            flag = false;
        }
        else {
            $("#valStateProvinceErr").html("");
        }
        var ZipCode = $("#valZipCode").val().trim();
        if (ZipCode.length < 1) {
            $("#valZipCodeErr").html("Zip Code is required.");
            flag = false;
        }
        else {
            $("#valZipCodeErr").html("");
        }
        var CellPhoneNumber = $("#valCellPhone").val();
        if (CellPhoneNumber.length < 1) {
            $("#valCellPhoneErr").html("Cell Phone Number is required.");
            flag = false;
        }
        else {
            $("#valCellPhoneErr").html("");
        }
        var email = $("#valEmailAddress").val();
        if (email.length < 1) {
            $("#valEmailAddressErr").html("Email Address is required.");
            flag = false;
        }
        else {
            $("#valEmailAddressErr").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                $("#valEmailAddressErr").html(ZnodeBase.prototype.getResourceByKeyName("ErrorEmailAddress"));
                flag = false;
            }
        }
        return flag;
    };
    CaseRequest.prototype.ValidationForQuoteLookupForm = function () {
        var flag = true;
        var QuoteNumber = $("#valNumber").val();
        if (QuoteNumber.length < 1) {
            $("#valNumberErr").html("Quote Number");
            flag = false;
        }
        else {
            $("#valNumberErr").html("");
        }
        var Qemail = $("#valEmailAddress").val();
        if (Qemail.length < 1) {
            $("#valEmailAddressErr").html("Email Address on Quote");
            flag = false;
        }
        else {
            $("#valEmailAddressErr").html("");
            var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(Qemail)) {
                $("#valEmailAddressErr").html("Enter Valid Email Address on Quote");
                flag = false;
            }
        }
        return flag;
    };
    return CaseRequest;
}(ZnodeBase));
//# sourceMappingURL=CaseRequest.js.map