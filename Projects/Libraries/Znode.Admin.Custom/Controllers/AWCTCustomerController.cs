﻿using System.Web.Mvc;
using Znode.Engine.Admin.Agents;
using Znode.Engine.Admin.Controllers;
using Znode.Engine.Admin.ViewModels;

namespace Znode.Admin.Custom.Controllers
{
    public class AWCTCustomerController : CustomerController
    {

        #region Public Constructor
        public AWCTCustomerController(ICustomerAgent customerAgent, IGiftCardAgent giftCardAgent, IOrderAgent orderAgent, IUserAgent userAgent, ICartAgent cartAgent)
            : base(customerAgent, giftCardAgent, orderAgent, userAgent, cartAgent)
        {

        }
        #endregion

        // This method update the users account details.
        [HttpPost]
        public override ActionResult CustomerEdit(CustomerViewModel model)
        {
            ModelState["UserName"].Errors.Clear();
            return base.CustomerEdit(model);
        }
    }
}
