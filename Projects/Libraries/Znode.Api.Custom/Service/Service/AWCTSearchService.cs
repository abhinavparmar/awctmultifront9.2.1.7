﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Custom.Helper;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTSearchService : SearchService
    {
        #region Private Variables.

        PublishProductHelper publishProductHelper = new PublishProductHelper();
        AWCTPublishedProdHelper publishProdHelper = new AWCTPublishedProdHelper();


        public AWCTSearchService() : base()
        {
        }
        #endregion

        #region Constructor

        #endregion

        #region Elastic search
        //Get search results.
        public override KeywordSearchModel FullTextSearch(SearchRequestModel model, NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            int portalId, catalogId, localeId;
            GetParametersValueForFilters(filters, out portalId, out catalogId, out localeId);
            expands.Remove(ZnodeConstant.Pricing);
            model.PageSize = 40;
            sorts.Add("DisplayOrder", "-1");
            KeywordSearchModel searchResult = base.FullTextSearch(model, expands, filters, sorts, page);

            if (searchResult.Products != null)
            {
                AWCTAttributeSwatchHelper attributeSwatchHelper = new AWCTAttributeSwatchHelper();
                attributeSwatchHelper.GetAssociatedConfigurableProducts(searchResult, model, "awctColorName");
            }
          
            #region Commented Code swatch code
            //ImageHelper image = new ImageHelper(portalId);
            //string ProductImageName = "";

            //string ImageSmallPath = image.GetImageHttpPathSmall(ProductImageName);
            //string OriginalImagepath = image.GetOriginalImagepath(ProductImageName);

            //string _imagePath = "";
            //string _swatchImagePath = "";


            //int index = 0;

            ////ImageSmallPath = http://localhost:44762/Data/Media/Catalog/7/400/
            //index = ImageSmallPath.LastIndexOf('/');
            //if (index != -1)
            //    _imagePath = ImageSmallPath.Substring(0, index) + "/";

            ////OriginalImagepath = http://localhost:44762/Data/Media/
            //index = OriginalImagepath.LastIndexOf('/');
            //if (index != -1)
            //{
            //    _swatchImagePath = OriginalImagepath.Substring(0, index) + "/";
            //    _imagePath = OriginalImagepath.Substring(0, index) + "/";
            //}
            //if (searchResult.Products != null)
            //{
            //    List<WebStoreGroupProductModel> simage = GetSwatchImages(String.Join(",", searchResult.Products.Select(x => x.ZnodeProductId).ToList()), _imagePath, _swatchImagePath, "PLP");
            //    foreach (SearchProductModel prd in searchResult.Products)
            //    {

            //        if (prd.AssociatedGroupProducts == null)
            //            prd.AssociatedGroupProducts = new List<WebStoreGroupProductModel>();
            //        prd.AssociatedGroupProducts = simage.Where(x => x.PublishProductId == prd.ZnodeProductId).Select
            //            (t => new WebStoreGroupProductModel()
            //            {
            //                ImageMediumPath = image.GetImageHttpPathSmall(t.ImageMediumPath),
            //                ImageThumbNailPath = t.ImageThumbNailPath
            //            }).ToList<WebStoreGroupProductModel>();
            //    }
            //}
            #endregion

            return searchResult;

        }
        public override void BindProductDetails(KeywordSearchModel searchResult, int portalId, IList<PublishCategoryProductDetailModel> productDetails)
        {
            try
            {


                ImageHelper imageHelper = new ImageHelper(portalId);

                searchResult?.Products?.ForEach(product =>
                {
                    ZnodeLogging.LogMessage("INNER SKU" + product.SKU + "--Prductimage DB-" + Convert.ToString(product.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);

                    PublishCategoryProductDetailModel productSKU = productDetails?
                                .FirstOrDefault(productdata => productdata.SKU == product.SKU);

                    if (IsNotNull(productSKU))
                    {
                        product.SalesPrice = productSKU.SalesPrice;
                        product.RetailPrice = productSKU.RetailPrice;
                        product.CurrencyCode = productSKU.CurrencyCode;
                        product.CultureCode = productSKU.CultureCode;
                        product.CurrencySuffix = productSKU.CurrencySuffix;
                        product.Quantity = productSKU.Quantity;
                        product.ReOrderLevel = productSKU.ReOrderLevel;
                        product.Rating = productSKU.Rating;
                        product.TotalReviews = productSKU.TotalReviews;
                        product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(product.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
                        //ZnodeLogging.LogMessage("SKU" + product.SKU + "--ImageSmallPath DB-" + Convert.ToString(product.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
                        //ZnodeLogging.LogMessage("SKU" + product.SKU + "--ImageSmallPath DB-" + Convert.ToString(product.ImageSmallPath), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
                    }
                    product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(product.Attributes.Where(y => y.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValues);
                });
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message, ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            }
        }
        #endregion
        private static void GetParametersValueForFilters(FilterCollection filters, out int portalId, out int catalogId, out int localeId)
        {
            int.TryParse(filters.Where(x => x.FilterName.Equals(FilterKeys.PortalId, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out portalId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.ZnodeCatalogId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out catalogId);
            int.TryParse(filters.Where(x => x.FilterName.Equals(WebStoreEnum.LocaleId.ToString(), StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault()?.FilterValue, out localeId);
        }
        private List<WebStoreGroupProductModel> GetSwatchImages(string PublishedProductId, string ImagePath, string SwatchPath, string PageName)
        {
            IZnodeViewRepository<WebStoreGroupProductModel> objStoredProc = new ZnodeViewRepository<WebStoreGroupProductModel>();

            objStoredProc.SetParameter("@PublishedProductList", PublishedProductId, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@ImagePath", ImagePath, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SwatchPath", SwatchPath, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Case", PageName, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("AWCT_GetSwatchImages @PublishedProductList,@ImagePath,@SwatchPath,@Case").ToList();

        }
    }
}
