﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;

namespace Znode.Api.Custom.Service.Service
{
    public class AWCTShoppingCartService : ShoppingCartService
    {

        private readonly IShoppingCartMap _shoppingCartMap;
        public int duty = Convert.ToInt32(ConfigurationManager.AppSettings["DutyCanada"]);

        public AWCTShoppingCartService()
        {
            publishProductHelper = GetService<IPublishProductHelper>();
            _shoppingCartMap = GetService<IShoppingCartMap>();
        }
        public override ShoppingCartModel Calculate(ShoppingCartModel shoppingCartModel)
        {

            ShoppingCartModel calculatedModel = null;

            ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
            ProfileModel profile = promotionHelper.GetProfileCache();
            int profileId = 0;
            if (HelperUtility.IsNotNull(profile))
            {
                profileId = profile.ProfileId;
            }

            if (shoppingCartModel.ShippingAddress != null)
            {
                if (shoppingCartModel.ShippingAddress.StateName != null)
                {
                    decimal cost = Convert.ToDecimal(GetShippingRate(shoppingCartModel));
                    shoppingCartModel.CustomShippingCost = cost;
                    shoppingCartModel.ShippingCost = cost;
                }
            }
            if (shoppingCartModel.Custom5 == "Remove" && shoppingCartModel.ShippingCost == 0)
            {
                decimal? subtotal = shoppingCartModel.SubTotal;
                shoppingCartModel.SubTotal = 1200;
                decimal cost = Convert.ToDecimal(GetShippingRate(shoppingCartModel));
                shoppingCartModel.CustomShippingCost = cost;
                shoppingCartModel.ShippingCost = cost;
                shoppingCartModel.SubTotal = subtotal;
            }
            shoppingCartModel.ProfileId = profileId;
            shoppingCartModel.IsAllowWithOtherPromotionsAndCoupons = DefaultGlobalConfigSettingHelper.IsAllowWithOtherPromotionsAndCoupons;
            calculatedModel = base.Calculate(shoppingCartModel);
            if (calculatedModel.ShippingAddress.CountryName == "CA")
            {
                calculatedModel.TotalAdditionalCost = Math.Round(Convert.ToDecimal(calculatedModel.ShoppingCartItems.Where(a => a.Custom2 == "True").Sum(x => (x.Quantity * x.UnitPrice * duty) / 100)),2);
            }
            //if (calculatedModel.Coupons.Count > 0)
            //{
            //    ZnodeShoppingCart znodeShoppingCart = _shoppingCartMap.ToZnodeShoppingCart(calculatedModel);
            //    ZnodeTaxManager taxManager = new ZnodeTaxManager(znodeShoppingCart);
            //    taxManager.Calculate(znodeShoppingCart);
            //    calculatedModel = _shoppingCartMap.ToModel(znodeShoppingCart, GetService<IImageHelper>(new ZnodeNamedParameter("PortalId", znodeShoppingCart.PortalId.GetValueOrDefault())));
            //}
            return calculatedModel;
        }
        public override ShippingListModel GetShippingEstimates(string zipCode, ShoppingCartModel model)
        {
            try
            {
                ShippingListModel slist = base.GetShippingEstimates(zipCode, model);
                decimal cost = 0;
                if (slist.ShippingList.Count > 0)
                {
                    try
                    {
                        //cost = Convert.ToDecimal(model.ShippingCost);
                        //if (cost == 0)
                        //{
                        if (model.ShippingAddress != null)
                        {
                            cost = Convert.ToDecimal(GetShippingRate(model));
                        }
                        //}
                        if (model.Custom5 == "Remove" && model.ShippingCost == 0)
                        {
                            decimal? subtotal = model.SubTotal;
                            model.SubTotal = 1200;
                            cost = Convert.ToDecimal(GetShippingRate(model));
                            model.CustomShippingCost = cost;
                            model.ShippingCost = cost;
                            model.SubTotal = subtotal;
                        }
                    }
                    catch (Exception)
                    {
                        if (model.ShippingAddress != null)
                        {
                            cost = Convert.ToDecimal(GetShippingRate(model));
                            //model.ShippingCost = cost;
                            //model.OrderLevelShipping += cost;
                        }
                    }
                    slist.ShippingList[0].ShippingRate = cost;

                    //return new ShippingListModel { ShippingList = list };
                    return slist;
                }
                else
                {
                    return new ShippingListModel { ShippingList = new List<ShippingModel>() };
                }
            }
            catch (Exception)
            {
                return new ShippingListModel { ShippingList = new List<ShippingModel>() };
            }

        }
        private string GetShippingRate(ShoppingCartModel shoppingCartModel)
        {
            IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();

            objStoredProc.SetParameter("@State", string.IsNullOrEmpty(shoppingCartModel.ShippingAddress.StateName) ? string.Empty : shoppingCartModel.ShippingAddress.StateName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@Country", shoppingCartModel.ShippingAddress.CountryName, ParameterDirection.Input, DbType.String);
            objStoredProc.SetParameter("@SubTotal", shoppingCartModel.SubTotal, ParameterDirection.Input, DbType.Decimal);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("Nivi_AWCTShippingRate @State,@Country,@SubTotal").Take(1).First();

        }
        private readonly IPublishProductHelper publishProductHelper;

        //public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        //{

        //    string shoppingCartCustomText = "";
        //    var personaliseAttr = shoppingCart.ShoppingCartItems[0].PersonaliseValuesList.Where(x => x.Key.Contains("filename")).Select(y => y.Value).First()??null;

        //    // shoppingCartCustomText = Convert.ToString(personaliseAttr.Value);
        //    //if (string.IsNullOrEmpty(personaliseAttr.Value.First().ToString()))
        //    //{
        //    //    string[] personalise = personaliseAttr.Value.First().ToString().Split('~');
        //    //    shoppingCartCustomText = personalise[1] ?? null;


        //    //}
        //    shoppingCartCustomText = Convert.ToString(personaliseAttr);
        //    if (!string.IsNullOrEmpty(shoppingCartCustomText))
        //    {
        //        long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
        //        string fileName = "UserId_" + shoppingCart.UserId + "_" + milliseconds.ToString() + ".Svg";
        //        SaveCustomSvgImage(shoppingCartCustomText, fileName);
        //    }

        //    base.AddToCartProduct(shoppingCart);

        //    return shoppingCart;
        //}

        public override AddToCartModel AddToCartProduct(AddToCartModel shoppingCart)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();

            ///Nivi:added to remove DataURi String from PersonaliseValuesList item and get it to save the svg file

            if (shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Count() > 0)
            {
                KeyValuePair<string, object>? personaliseValue = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("Value"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonFileUriValue = js.Serialize(personaliseValue);
                PersonaliseData personalizeOptionValue = js.Deserialize<PersonaliseData>(jsonFileUriValue);
                personalizeOptionValue.Value = personalizeOptionValue.Value.ToString().Replace("\"", "").Replace(":", "").Replace("Value", "");
                //personalizeOptionValue.Value = personalizeOptionValue.Value.ToString().Replace("\"", "").Replace("Value", "");



                KeyValuePair<string, object>? personaliseSku = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("sku"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonPersonaliseSku = js.Serialize(personaliseSku);
                PersonaliseData personaliseClassSku = js.Deserialize<PersonaliseData>(jsonPersonaliseSku);
                personaliseClassSku.Value = personaliseClassSku.Value.ToString().Replace("\"", "").Replace(":", "").Replace("sku", "").Replace("{", "");
                personaliseClassSku.Value = personaliseClassSku.Value.Replace("{PersonaliseItems[{", "").Replace("PersonaliseItems[", "");

                KeyValuePair<string, object>? personaliseFileUriValue = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("FileUriValue"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonSerializeFileUriValue = js.Serialize(personaliseFileUriValue);
                PersonaliseData personalizeClassFileUriValue = js.Deserialize<PersonaliseData>(jsonSerializeFileUriValue);
                personalizeClassFileUriValue.Value = personalizeClassFileUriValue.Value.ToString().Replace("\"", "").Replace(":", "").Replace("Filename", "");

                KeyValuePair<string, object>? personaliseFilename = shoppingCart.ShoppingCartItems[0]?.PersonaliseValuesList?.Where(x => x.Key.Contains("Filename"))?.ToDictionary(x => x.Key, x => x.Value)?.FirstOrDefault();
                string jsonFilename = js.Serialize(personaliseFilename);
                PersonaliseData personaliseClassFilename = js.Deserialize<PersonaliseData>(jsonFilename);
                personaliseClassFilename.Value = personaliseClassFilename.Value.ToString().Replace("\"", "").Replace(":", "").Replace("Filename", "").Replace("}]", "");

                shoppingCart.ShoppingCartItems.ToList().ForEach(s =>
                {
                    s.PersonaliseValuesList.Remove("Value");
                    s.PersonaliseValuesList.Remove("sku");
                    s.PersonaliseValuesList.Remove("Filename");
                    s.PersonaliseValuesList.Remove("FileUriValue");

                });

                string SkuString1 = personaliseClassSku.Value.ToString().Replace("\"", "").Replace("PersonaliseItems", "").Replace("{:[{", "").Replace("sku:", "");

                HashSet<string> validNames = new HashSet<string>(SkuString1.Split('|'));
                shoppingCart.ShoppingCartItems.Where(x => validNames.Contains(x.ConfigurableProductSKUs)).ToList().ForEach(s =>
                {
                    s.PersonaliseValuesList.Add("OptionValue", personalizeOptionValue.Value);
                    s.PersonaliseValuesList.Add("sku", personaliseClassSku.Value);
                    s.PersonaliseValuesList.Add("Filename", personaliseClassFilename.Value);
                });
                ///Nivi:End


                personalizeClassFileUriValue.Value = personalizeClassFileUriValue.Value.ToString().Replace("\"", "").Replace("FileUriValue", "");
                string shoppingCartCustomText = personalizeClassFileUriValue.Value;
                if (!string.IsNullOrEmpty(shoppingCartCustomText))
                {
                    SaveCustomSvgImage(shoppingCartCustomText, personaliseClassFilename.Value.ToString());
                }
            }
            //999999347725-Dance|999999347732-Dance|
            string cartsku = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom1);
            cartsku = cartsku.Substring(2, cartsku.Length - 4);
            string[] skus = cartsku.Split('|');

            string awctcatalogName = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom2);
            awctcatalogName = awctcatalogName.Substring(2, awctcatalogName.Length - 4);
            string[] awctcatalogNames = awctcatalogName.Split('|');

            string Isdutiable = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom3);
            Isdutiable = Isdutiable.Substring(2, Isdutiable.Length - 4);
            string[] Isdutiables = Isdutiable.Split('|');

            string date = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom4);
            date = date.Substring(2, date.Length - 4);
            string[] dates = date.Split('|');
            foreach (ShoppingCartItemModel cartItems in shoppingCart?.ShoppingCartItems)
            {
                int icount = 0;
                foreach (string sku in skus)
                {
                    if (cartItems.ConfigurableProductSKUs == sku)
                    {
                        cartItems.Custom1 = awctcatalogNames[icount];
                        cartItems.Custom2 = Isdutiables[icount];
                        cartItems.Custom3 = dates[icount];
                        cartItems.CustomText = cartItems.Custom1;
                    }
                    icount = icount + 1;
                }
            }
            //string IsDutiable = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom2);
            //IsDutiable = IsDutiable.Substring(2, IsDutiable.Length - 4);
            //shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom2 = IsDutiable);
            //shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom1 = awctcatalogName);

            //string awctColorNumEstimateDate = Newtonsoft.Json.JsonConvert.SerializeObject(shoppingCart.Custom4);
            //shoppingCart?.ShoppingCartItems.ToList().ForEach(cc => cc.Custom4 = awctColorNumEstimateDate);

            base.AddToCartProduct(shoppingCart);

            return shoppingCart;
        }

        //To get ShoppingCart by cookieId 
        //public override ShoppingCartModel GetShoppingCartDetails(CartParameterModel cartParameterModel, ShoppingCartModel cartModel = null)
        //{
        //    ZnodeShoppingCart shopping = new ZnodeShoppingCart();
        //    ZnodeShoppingCart znodeShoppingCart = shopping.LoadFromDatabase(cartParameterModel);
        //    znodeShoppingCart.IsAllowWithOtherPromotionsAndCoupons = DefaultGlobalConfigSettingHelper.IsAllowWithOtherPromotionsAndCoupons;
        //    //to map cart model to znode shopping cart  
        //    BindCartModel(cartModel, znodeShoppingCart);

        //    //Map Libraries.ECommerce.ShoppingCart to ShoppingCartModel.
        //    ShoppingCartModel shoppingCartModel = Engine.Services.Maps.ShoppingCartMap.ToModel(znodeShoppingCart);

        //    //Get coupons if already applied.
        //    if (cartModel?.Coupons?.Count > 0)
        //        shoppingCartModel.Coupons = cartModel.Coupons;

        //    if (HelperUtility.IsNotNull(cartParameterModel.ShippingId))
        //    {
        //        IZnodeRepository<ZnodeShipping> _shippingRepository = new ZnodeRepository<ZnodeShipping>();

        //        //Check if Shipping is null or not,If null then get the shipping 
        //        //on the basis of ShippingId form ShoppingCartModel.
        //        ZnodeShipping shipping = _shippingRepository.Table.Where(x => x.ShippingId == cartParameterModel.ShippingId)?.FirstOrDefault();
        //        if (HelperUtility.IsNotNull(shipping))
        //        {
        //            shoppingCartModel.Shipping = new OrderShippingModel
        //            {
        //                ShippingId = shipping.ShippingId,
        //                ShippingDiscountDescription = shipping.Description,
        //                ShippingCountryCode = string.IsNullOrEmpty(cartParameterModel.ShippingCountryCode) ? string.Empty : cartParameterModel.ShippingCountryCode
        //            };
        //        }
        //    }

        //    //Check product inventory of the product for all type of product in cartline item.
        //    if (HelperUtility.IsNotNull(shoppingCartModel?.ShoppingCartItems))
        //    {
        //        if (cartModel?.ShoppingCartItems?.Count > 0)
        //        {
        //            shoppingCartModel.ShoppingCartItems.ForEach(cartItem =>
        //            {
        //                var lineItem = cartModel.ShoppingCartItems
        //                            .Where(cartLineItem => cartLineItem.SKU == cartItem.SKU && cartLineItem.AddOnProductSKUs == cartItem.AddOnProductSKUs)?.FirstOrDefault();
        //                cartItem.OmsOrderLineItemsId = (lineItem?.OmsOrderLineItemsId).GetValueOrDefault();
        //                cartItem.CartDescription = string.IsNullOrEmpty(cartItem.CartDescription) ? lineItem?.CartDescription : cartItem.CartDescription;

        //            });
        //        }

        //        //Single call instead multiple inventory check.
        //        CheckBaglineItemInventory(shoppingCartModel, cartParameterModel);
        //    }

        //    //Bind cookieMappingId, PortalId, LocaleId, CatalogId, UserId.
        //    BindCartData(shoppingCartModel, cartParameterModel);

        //    if (cartModel?.OmsOrderId > 0)
        //        shoppingCartModel.BillingAddress = cartModel.BillingAddress;

        //    return shoppingCartModel;
        //}
        private void SaveCustomSvgImage(string svgDataUri, string fileName)
        {
            try
            {
                svgDataUri = svgDataUri.Replace("$$", ";");
                string path;
                string base64Data = Regex.Match(svgDataUri, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                byte[] binData = Convert.FromBase64String(svgDataUri);
                path = HttpContext.Current.Server.MapPath("~") + ConfigurationManager.AppSettings["SVGPath"].ToString();
                fileName = Path.Combine(path, fileName);
                if (!File.Exists(fileName))
                {

                    File.WriteAllBytes(fileName, binData);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("SaveSVGImage.Error" + ex.Message, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
        }

        public Dictionary<string, object> GetPersonalisedValueCartLineItem(int savedCartLineItemId, int localeId)
        {
            IZnodeRepository<ZnodeOmsPersonalizeCartItem> _savedOmsPersonalizeCartItemRepository = new ZnodeRepository<ZnodeOmsPersonalizeCartItem>();

            IZnodeRepository<ZnodeOmsSavedCartLineItem> _omsSavedCartLineItem = new ZnodeRepository<ZnodeOmsSavedCartLineItem>();
            int OmsSavedCartLineItemId = (from p in _omsSavedCartLineItem.Table
                                          where p.ParentOmsSavedCartLineItemId == savedCartLineItemId
                                          select p.OmsSavedCartLineItemId).FirstOrDefault();
            int iSavedCartLineItemId = Convert.ToInt32(OmsSavedCartLineItemId);
            Dictionary<string, object> personaliseItem = new Dictionary<string, object>();
            foreach (KeyValuePair<string, string> personaliseAttr in _savedOmsPersonalizeCartItemRepository.Table.Where(x => x.OmsSavedCartLineItemId == iSavedCartLineItemId)?.ToDictionary(x => x.PersonalizeCode, x => x.PersonalizeValue))
            {
                personaliseItem.Add(personaliseAttr.Key, (object)personaliseAttr.Value);
            }

            return personaliseItem;
        }
        #region To check product inventory 9.2.1.1
        ////To check product inventory 9.2.1.1
        //public override void CheckBaglineItemInventory(ShoppingCartModel shoppingCartModel, CartParameterModel cartParameterModel)
        //{
        //    ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //    List<ShoppingCartItemModel> shoppingCartItemList = HelperUtility.IsNotNull(shoppingCartModel) ? shoppingCartModel.ShoppingCartItems : new List<ShoppingCartItemModel>();
        //    ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { shoppingCartItemListCount = shoppingCartItemList });

        //    string allSku = string.Empty;
        //    shoppingCartItemList.ForEach(shoppingCartItem =>
        //    {
        //        List<string> skus = new List<string>();
        //        List<string> productSkus = new List<string>();
        //        //Get all associated skus of items.
        //        GetAllItemsSku(shoppingCartItem, productSkus, out allSku);
        //        skus.AddRange(productSkus);
        //        skus = skus?.Distinct().ToList();
        //        //Get the inventory list of skus.
        //        List<InventorySKUModel> inventoryList = skus.Count > 0 ? publishProductHelper.GetInventoryBySKUs(skus, shoppingCartModel.PortalId) : new List<InventorySKUModel>();
        //        ZnodeLogging.LogMessage("Parameter:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { skus = skus });
        //        //Get inventory setting details from publish products.
        //        List<ProductEntity> invertorySetting = new List<ProductEntity>();
        //        foreach (var item in skus)
        //        {
        //            ProductEntity Products = publishProductHelper.GetPublishProductBySKUs(item, cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId, GetCatalogVersionId(cartParameterModel.PublishedCatalogId)).FirstOrDefault();
        //            invertorySetting.Add(Products);
        //        }
        //        decimal selectedQuantity = new decimal();

        //        //Get insufficient Quantity Count of current cart item.
        //        int insufficientQuantityCount = GetCartInsufficientQuantityCount(shoppingCartItem, skus, ref selectedQuantity, inventoryList, invertorySetting);

        //        //if insufficient Quantity Count is greateer then 0 then current cart is out of stock.
        //        if (insufficientQuantityCount > 0)
        //            shoppingCartItem.InsufficientQuantity = true;
        //    });
        //    ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
        //}
        #endregion

        //Get insufficient Quantity Count of current cart item.
        //To check product inventory 9.1.0.9
        public override void CheckBaglineItemInventory(ShoppingCartModel shoppingCartModel, CartParameterModel cartParameterModel)
        {

            List<ShoppingCartItemModel> shoppingCartItemList = HelperUtility.IsNotNull(shoppingCartModel) ? shoppingCartModel.ShoppingCartItems : new List<ShoppingCartItemModel>();

            List<string> skus = new List<string>();
            string allSku = string.Empty;

            shoppingCartItemList.ForEach(shoppingCartItem =>
            {

                List<string> productSkus = new List<string>();
                //Get all associated skus of items.
                GetAllItemsSku(shoppingCartItem, productSkus, out allSku);
                skus.AddRange(productSkus);
                skus = skus?.Distinct().ToList();
            });

            //Get the inventory list of skus.
            List<InventorySKUModel> inventoryList = skus.Count > 0 ? publishProductHelper.GetInventoryBySKUs(skus, shoppingCartModel.PortalId) : new List<InventorySKUModel>();

            //Get inventory setting details from publish products.
            List<ProductEntity> invertorySetting = publishProductHelper.GetPublishProductBySKUs(allSku, cartParameterModel.PublishedCatalogId, cartParameterModel.LocaleId, GetCatalogVersionId(cartParameterModel.PublishedCatalogId));

            shoppingCartItemList.ForEach(shoppingCartItem =>
            {

                decimal selectedQuantity = shoppingCartItem.Quantity;

                //Get insufficient Quantity Count of current cart item.
                int insufficientQuantityCount = GetCartInsufficientQuantityCount(shoppingCartItem, skus, ref selectedQuantity, inventoryList, invertorySetting);

                //if insufficient Quantity Count is greateer then 0 then current cart is out of stock.
                if (insufficientQuantityCount > 0)
                {
                    shoppingCartItem.InsufficientQuantity = true;
                }
            });
        }
        private int GetCartInsufficientQuantityCount(ShoppingCartItemModel shoppingCartItem, List<string> skus, ref decimal selectedQuantity, List<InventorySKUModel> inventoryList, List<ProductEntity> invertorySetting)
        {
            ZnodeLogging.LogMessage("Execution started:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            int insufficientQuantityCount = 0;
            foreach (ProductEntity products in invertorySetting)
            {

                //Check for group product inventory.
                if (shoppingCartItem?.GroupProducts?.Count > 0)
                {

                    foreach (AssociatedProductModel associatedProduct in shoppingCartItem.GroupProducts)
                    {
                        if (string.Equals(associatedProduct.Sku, products.SKU))
                        {
                            selectedQuantity = associatedProduct.Quantity;

                            //Get insufficient Quantity Count of current item.
                            insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryList, insufficientQuantityCount, products);
                        }
                    }
                }
                //Check for Configurable product inventory.
                else if (!string.IsNullOrEmpty(shoppingCartItem?.ConfigurableProductSKUs))
                {
                    List<InventorySKUModel> inventoryListForConfigProd = inventoryList.Where(w => w.SKU == shoppingCartItem.ConfigurableProductSKUs).ToList();
                    if (string.Equals(products.SKU, shoppingCartItem.ConfigurableProductSKUs))
                    {
                        insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryListForConfigProd, insufficientQuantityCount, products);
                    }
                }
                //Check for Bundle product inventory.
                else if (!string.IsNullOrEmpty(shoppingCartItem?.BundleProductSKUs))
                {
                    foreach (string BundleSku in skus)
                    {
                        if (string.Equals(products.SKU, BundleSku))
                        {
                            List<InventorySKUModel> inventoryListForBundleProd = inventoryList.Where(w => w.SKU == BundleSku).ToList();
                            insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryListForBundleProd, insufficientQuantityCount, products);

                        }
                    }
                }
                //Check for Simple product inventory.
                else
                {
                    //Get insufficient Quantity Count of current item.
                    insufficientQuantityCount = IsItemOutOfStock(shoppingCartItem, skus, selectedQuantity, inventoryList, insufficientQuantityCount, products);
                }
            }
            ZnodeLogging.LogMessage("Insufficient quantity count:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, insufficientQuantityCount);
            ZnodeLogging.LogMessage("Execution done:", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            return insufficientQuantityCount;
        }

    }
    public class PersonaliseData
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
