﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Libraries.ECommerce.Entities;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;

namespace Znode.Api.Custom.Promotions
{
    public class AWCTZnodeCartPromotionPercentOffOrder : ZnodeCartPromotionType
    {
        #region Constructor
        public AWCTZnodeCartPromotionPercentOffOrder()
        {
            Name = "AWCT Percent Off Order";
            Description = "Applies a percent off an entire order; affects the shopping cart.";
            AvailableForFranchise = false;

            Controls.Add(ZnodePromotionControl.Store);
            Controls.Add(ZnodePromotionControl.Profile);
            Controls.Add(ZnodePromotionControl.DiscountPercent);
            Controls.Add(ZnodePromotionControl.MinimumOrderAmount);
            Controls.Add(ZnodePromotionControl.Coupon);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Calculates the percent off an order.
        /// </summary>
        public override void Calculate(int? couponIndex, List<PromotionModel> allPromotions)
        {
            ZnodeLogging.LogMessage("GetCart-Calculate Promotions-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
            ProfileModel profile = promotionHelper.GetProfileCache();
            int profileId = 0;
            if (HelperUtility.IsNotNull(profile))
            {
                profileId = profile.ProfileId;
            }

            decimal subTotal = ShoppingCart.SubTotal;
            subTotal = GetCartSubTotal(ShoppingCart);
            OrderBy = nameof(PromotionModel.OrderMinimum);
            ApplicablePromolist = GetPromotionsByType(ZnodeConstant.PromotionClassTypeCart, ClassName, allPromotions, OrderBy, Convert.ToInt32(ShoppingCart.PortalId));
            PromotionModel Promolist = null;
            if (profileId != 2)
            {
                Promolist = ApplicablePromolist.OrderBy(o => o.OrderMinimum).FirstOrDefault(x => x.OrderMinimum <= subTotal && x.ProfileId == profileId);
                if (Promolist == null)
                {
                    // Promolist = ApplicablePromolist.OrderBy(o => o.OrderMinimum).FirstOrDefault(x => x.OrderMinimum <= subTotal);
                    Promolist = ApplicablePromolist.FirstOrDefault(x => x.OrderMinimum <= subTotal);

                }
            }
            else
            {
                Promolist = ApplicablePromolist.FirstOrDefault(x => x.OrderMinimum <= subTotal);
            }
            // Promolist.IsAllowWithOtherPromotionsAndCoupons = true;//ShoppingCart.IsAllowWithOtherPromotionsAndCoupons;
            if (Equals(PromotionBag.Coupons, null))
            {
                if (!ShoppingCart.IsAnyPromotionApplied)
                {
                    if (PromotionBag.MinimumOrderAmount <= subTotal)
                    {
                        ZnodeLogging.LogMessage("Calculate " + PromotionBag.PromoCode + "-Time:-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                        ZnodeLogging.LogMessage("Calculate " + subTotal.ToString() + "-Time:-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                        if (!IsApplicablePromotion(new List<PromotionModel>() { Promolist }, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                        {
                            // if (!ZnodePromotionHelper.IsApplicablePromotion(new List<PromotionModel>() { Promolist }, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, false))
                            return;
                        }

                        decimal discount = PromotionBag.Discount / 100;
                        ZnodeLogging.LogMessage("Calculate " + discount.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                        ApplyDiscount(discount);
                        ShoppingCart.IsAnyPromotionApplied = true;
                        ShoppingCart.Custom3 = discount.ToString();
                        ZnodeLogging.LogMessage("RemoveDiscount Custom5" + Convert.ToString(ShoppingCart.Custom5) + "-Time:-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                        if (Convert.ToString(ShoppingCart.Custom5) == "Remove")
                        {
                            ShoppingCart.Custom3 = "-1";
                            RemoveDiscount(discount);
                            ShoppingCart.IsAnyPromotionApplied = false;
                        }
                    }
                    else
                    {
                        decimal discount = PromotionBag.Discount / 100;
                        RemoveDiscount(discount);
                        ShoppingCart.Custom3 = "-1";
                        ShoppingCart.IsAnyPromotionApplied = false;
                    }
                }
            }
            else if (!Equals(PromotionBag.Coupons, null))
            {

                bool isCouponValid = ValidateCoupon(couponIndex);

                foreach (CouponModel coupon in PromotionBag.Coupons)
                {
                    if ((coupon.AvailableQuantity > 0 || IsExistingOrderCoupon(coupon.Code)) && CheckCouponCodeValid(ShoppingCart.Coupons[couponIndex.Value].Coupon, coupon.Code))
                    {
                        if (PromotionBag.MinimumOrderAmount <= subTotal && isCouponValid)
                        {
                            if (!ZnodePromotionHelper.IsApplicablePromotion(ApplicablePromolist, PromotionBag.PromoCode, ShoppingCart.SubTotal, true, true))
                            {
                                return;
                            }

                            decimal discount = PromotionBag.Discount / 100;
                            ApplyDiscount(discount, coupon.Code);
                            SetCouponApplied(coupon.Code);
                            ShoppingCart.Coupons[couponIndex.Value].CouponApplied = true;


                        }

                        if (IsUniqueCouponApplied(PromotionBag, ShoppingCart.Coupons[couponIndex.Value].CouponApplied))
                        {
                            break;
                        }
                    }
                }

                AddPromotionMessage(couponIndex);
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get shopping cart Sub total.
        /// </summary>
        /// <param name="currentItem">The current item in the shopping cart.</param>
        /// <returns>shopping cart subtotal</returns>
        public override decimal GetCartSubTotal(ZnodeShoppingCart ShoppingCart)
        {
            decimal subTotal = 0;
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                if (isExclusionCategory(cartItem.Custom1))
                {
                    continue;
                }
                subTotal += cartItem.PromotionalPrice * cartItem.Quantity;
            }
            return subTotal;
        }
        public static bool IsApplicablePromotion(List<PromotionModel> promotionList, string promoCode, decimal minValue, bool isOrderAmountBasedPromotion = false, bool isCoupon = false)
        {
            if (Convert.ToBoolean(promotionList.Any(a => a.IsCouponRequired == false && !a.IsAllowWithOtherPromotionsAndCoupons)))//
            {
                return false;
            }

            PromotionModel applicablePromotion = promotionList.FirstOrDefault(o => o.PromoCode.Equals(promoCode) &&
                                                (isOrderAmountBasedPromotion ? o.OrderMinimum <= minValue : o.QuantityMinimum <= minValue) && o.IsCouponRequired == isCoupon);

            return HelperUtility.IsNotNull(applicablePromotion);
        }
        private bool isExclusionCategory(string category)
        {
            string ExclusionCategory = ConfigurationManager.AppSettings["ExclusionCategory"].ToString();
            return ExclusionCategory.Split(',').Contains(category);
        }
        private void ApplyDiscount(decimal discount, string couponCode = "")
        {
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {
                ZnodeLogging.LogMessage("Calculate " + Convert.ToString(cartItem.Custom1) + "-Time:-" + DateTime.Now.ToString(), ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
                if (isExclusionCategory(cartItem.Custom1))
                {
                    continue;
                }

                decimal finalPrice = cartItem.PromotionalPrice;
                decimal lineItemDiscount = finalPrice * discount;

                if (IsDiscountApplicable(cartItem.Product.DiscountAmount, cartItem.Product.FinalPrice))
                {
                    cartItem.Product.DiscountAmount += lineItemDiscount;
                    cartItem.Product.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), lineItemDiscount, GetDiscountType(couponCode), cartItem.Product.OrdersDiscount);
                    SetPromotionalPriceAndDiscount(cartItem, lineItemDiscount);
                }

                foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                {
                    if (addon.FinalPrice > 0.0M)
                    {
                        lineItemDiscount = addon.FinalPrice * discount;
                        addon.DiscountAmount += lineItemDiscount;
                        addon.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), lineItemDiscount, GetDiscountType(couponCode), addon.OrdersDiscount);
                        if (lineItemDiscount > 0)
                        {
                            SetPromotionalPriceAndDiscount(cartItem, lineItemDiscount);
                        }
                    }
                }

                if (finalPrice == 0 && cartItem.Product.ZNodeConfigurableProductCollection.Count > 0)
                {
                    foreach (ZnodeProductBaseEntity config in cartItem.Product.ZNodeConfigurableProductCollection)
                    {
                        if (config.FinalPrice > 0.0M)
                        {
                            config.DiscountAmount += lineItemDiscount;
                            config.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), lineItemDiscount, GetDiscountType(couponCode), config.OrdersDiscount);
                        }
                    }
                }

                foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                {
                    if (group.FinalPrice > 0.0M)
                    {
                        lineItemDiscount = group.FinalPrice * discount;
                        group.DiscountAmount += lineItemDiscount;
                        group.OrdersDiscount = SetOrderDiscountDetails(GetDiscountCode(PromotionBag.PromoCode, couponCode), lineItemDiscount, GetDiscountType(couponCode), group.OrdersDiscount);
                        if (lineItemDiscount > 0)
                        {
                            SetPromotionalPriceAndDiscount(cartItem, lineItemDiscount);
                        }
                    }
                }
            }
        }

        private List<PromotionModel> GetPromotionsByType(string promotionsType, string promotionName, List<PromotionModel> allPromotions, string orderBy = "QuantityMinimum", int portalId = 0)
        {
            ZnodePromotionHelper promotionHelper = new ZnodePromotionHelper();
            ProfileModel profile = promotionHelper.GetProfileCache();
            int profileId = 0;
            if (HelperUtility.IsNotNull(profile))
            {
                profileId = profile.ProfileId;
            }
            //List<PromotionModel> promotions = allPromotions.Where(promo => (DateTime.Today.Date >= promo.StartDate && DateTime.Today.Date <= promo.EndDate)
            //                                                                && promo.PromotionType.ClassType.Equals(promotionsType, StringComparison.OrdinalIgnoreCase)
            //                                                                && promo.PromotionType.ClassName.Equals(promotionName)
            //                                                                && promo.PromotionType.IsActive && (promo.PortalId == portalId || promo.PortalId == null)
            //                                                               )
            //                                               .OrderByDescending(x => typeof(PromotionModel).GetProperty(orderBy).GetValue(x, null))
            //                                               .ToList();
            List<PromotionModel> promotions = allPromotions.Where(promotion => (DateTime.Today.Date >= promotion.StartDate && DateTime.Today.Date <= promotion.EndDate) &&
                            (promotion.ProfileId == profileId || promotion.ProfileId == null) &&
                            (promotion.PortalId == portalId || promotion.PortalId == null) &&
                            // (promotion.IsCouponRequired == false) &&
                            promotion.PromotionType.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.DisplayOrder)
                             .OrderByDescending(x => typeof(PromotionModel).GetProperty(orderBy).GetValue(x, null)).ToList();

            return promotions ?? new List<PromotionModel>();
            //&& (promo.ProfileId == ProfileId || promo.ProfileId == null)
        }
        private void RemoveDiscount(decimal discount)
        {
            if (Convert.ToString(ShoppingCart.Custom5) != "Remove")
            {
                return;
            }
            foreach (ZnodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
            {

                decimal finalPrice = cartItem.ExtendedPrice;
                decimal lineItemDiscount = finalPrice * discount;

                //if (cartItem.Product.DiscountAmount > lineItemDiscount)
                //    cartItem.Product.DiscountAmount -= lineItemDiscount;

                cartItem.Product.DiscountAmount = 0;// cartItem.Product.DiscountAmount < 0 ? 0 : cartItem.Product.DiscountAmount;

                foreach (ZnodeProductBaseEntity addon in cartItem.Product.ZNodeAddonsProductCollection)
                {
                    if (addon.FinalPrice > 0.0M)
                    {
                        lineItemDiscount = addon.FinalPrice * discount;
                        if (addon.DiscountAmount > lineItemDiscount)
                        {
                            addon.DiscountAmount -= lineItemDiscount;
                        }
                        addon.DiscountAmount = addon.DiscountAmount < 0 ? 0 : addon.DiscountAmount;
                    }
                }

                foreach (ZnodeProductBaseEntity group in cartItem.Product.ZNodeGroupProductCollection)
                {
                    if (group.FinalPrice > 0.0M)
                    {
                        lineItemDiscount = group.FinalPrice * discount;
                        if (group.DiscountAmount > lineItemDiscount)
                        {
                            group.DiscountAmount -= lineItemDiscount;
                        }
                        group.DiscountAmount = group.DiscountAmount < 0 ? 0 : group.DiscountAmount;
                    }
                }


            }
        }
        #endregion
    }
}
