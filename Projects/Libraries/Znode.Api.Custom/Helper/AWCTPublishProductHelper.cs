﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Helper
{
    public class AWCTPublishProductHelper : PublishProductHelper
    {
        public override void GetProductsSEOAndReviews(int portalId, List<SearchProductModel> productList, int localeId, int catalogVersionId)
        {
            ZnodeLogging.LogMessage("GetProductsSEOAndReviews-" + catalogVersionId.ToString(), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            base.GetProductsSEOAndReviews(portalId, productList, localeId, catalogVersionId);
        }
        //Map product data.
        protected override void MapSearchProductData(List<SearchProductModel> searchProducts, List<SeoEntity> details)
        {
            ZnodeLogging.LogMessage("MapSearchProductData", ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            if (searchProducts?.Count > 0)
            {
                int count = details.Count();
                ZnodeLogging.LogMessage("MapSearchProductData count--" + count.ToString(), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);

                searchProducts.ForEach(product =>
                {
                    ZnodeLogging.LogMessage("MapSearchProductData--" + product.SKU + ":", ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);

                    SeoEntity productDetails = details
                                .Where(productdata => productdata.SEOCode == product.SKU).FirstOrDefault();
                    // ZnodeLogging.LogMessage("MapSearchProductData SEO Code--" + Convert.ToString(productDetails.SEOCode), ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
                    if (HelperUtility.IsNotNull(productDetails))
                    {
                        ZnodeLogging.LogMessage("MapSearchProductData SEO--" + productDetails.SEOUrl, ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
                        product.SEODescription = productDetails.SEODescription;
                        product.SEOKeywords = productDetails.SEOKeywords;
                        product.SEOTitle = productDetails.SEOTitle;
                        product.SEOUrl = productDetails.SEOUrl;
                    }
                });
            }
        }

        //Get product customer reviews.
        public override void GetProductCustomerReviews(PublishProductModel publishProduct, int portalId)
        {
            int productId = publishProduct.ConfigurableProductId > 0 ? publishProduct.ConfigurableProductId : publishProduct.PublishProductId;
            //Get Promotions Associated to Publish Product Id
            publishProduct.ProductReviews = GetProductReviews(productId, portalId);

            //Get Product average rating from total product reviews.
            if (publishProduct?.ProductReviews?.Count > 0)
                publishProduct.Rating = Math.Round((decimal)publishProduct.ProductReviews.Sum(x => x.Rating) / publishProduct.ProductReviews.Count, 2);

        }
    }
}
