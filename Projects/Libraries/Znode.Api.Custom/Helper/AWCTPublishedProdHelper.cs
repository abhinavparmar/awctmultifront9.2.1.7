﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;


namespace Znode.Api.Custom.Helper
{
    public class AWCTPublishedProdHelper : ZnodeBusinessBase
    {
        #region Private Variables
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        #endregion

        #region Constructor
        public AWCTPublishedProdHelper()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
        }
        #endregion

        public List<ConfigurableProductEntity> GetConfigurableAllProductEntity(List<int> productids, int? catalogVersionId,int catalogId)
        {
            IMongoRepository<ConfigurableProductEntity> _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();
            //FilterCollection filters = GetConfigurableFilter(productList, catalogVersionId);
            List<IMongoQuery> qry = new List<IMongoQuery>();
            qry.Add(Query<ConfigurableProductEntity>.In(x => x.ZnodeProductId, productids));
            qry.Add(Query<ConfigurableProductEntity>.EQ(x => x.VersionId, catalogVersionId.GetValueOrDefault()));
            qry.Add(Query<ConfigurableProductEntity>.EQ(x => x.ZnodeCatalogId, catalogId));
            //Get Configurable Product
            List<ConfigurableProductEntity> configEntity = _configurableproductRepository.GetEntityList(Query.And(qry));
            return configEntity;
        }    

        public List<ProductEntity> GetAssociatedProducts(int localeId, int? catalogVersionId, List<ConfigurableProductEntity> configEntity)
        {
            //Check if entity is not null.
            if (HelperUtility.IsNotNull(configEntity))
            {              
                //Get associated product list.

                List<IMongoQuery> qry = new List<IMongoQuery>();
                qry.Add(Query<ProductEntity>.In(x => x.ZnodeProductId, configEntity?.Select(x => x.AssociatedZnodeProductId)?.ToArray()));
                qry.Add(Query<ProductEntity>.EQ(x => x.LocaleId, localeId));
                qry.Add(Query<ProductEntity>.EQ(x => x.VersionId, catalogVersionId));
                List<ProductEntity> associatedProducts = _ProductMongoRepository.GetEntityList(Query.And(qry));
                return associatedProducts;
            }
            return null;
        }
     
    }
}
