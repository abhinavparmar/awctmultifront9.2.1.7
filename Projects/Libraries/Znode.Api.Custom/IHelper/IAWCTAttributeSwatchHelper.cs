﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;

namespace Znode.Api.Custom.IHelper
{
    interface IAWCTAttributeSwatchHelper
    {
        void GetpublishedConfigurableProducts(List<PublishProductModel> publishProducts, int PortalId, string attributeCode);
    }
}
