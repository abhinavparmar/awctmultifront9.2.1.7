﻿using Autofac;
using Znode.Api.Client.Custom.Clients;
using Znode.Api.Client.Custom.Clients.Clients;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Framework.Business;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.Controllers;

namespace Znode.Engine.WebStore
{
    public class CustomDependancyRegistration : IDependencyRegistration
    {
        public virtual void Register(ContainerBuilder builder)
        {
            builder.RegisterType<CustomUserController>().As<UserController>().InstancePerDependency();
           
            builder.RegisterType<AWCTPublishProductClient>().As<IAWCTPublishProductClient>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTProductAgent>().As<IAWCTProductAgent>().InstancePerDependency();

            builder.RegisterType<AWCTPublishProductModel>().As<PublishProductModel>().InstancePerDependency();
            builder.RegisterType<AWCTProductController>().As<ProductController>().InstancePerDependency();
            builder.RegisterType<AWCTCheckoutAgent>().As<ICheckoutAgent>().InstancePerDependency();
            builder.RegisterType<AWCTCartAgent>().As<IAWCTCartAgent>().InstancePerDependency();
          //  builder.RegisterType<IAWCTCartAgent>().As<ICartAgent>().InstancePerDependency();
            builder.RegisterType<AWCTCheckoutController>().As<CheckoutController>().InstancePerDependency();
           

            builder.RegisterType<AWCTOrderClient>().As<OrderClient>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTOrderClient>().As<IOrderClient>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTUserAgent>().As<IUserAgent>().InstancePerDependency();

            builder.RegisterType<AWCTCartController>().As<CartController>().InstancePerDependency();

            //Customized Form
            builder.RegisterType<AWCTCustomizedFormClient>().As<IAWCTCustomizedFormClient>().InstancePerLifetimeScope();
            builder.RegisterType<AWCTCustomizedFormAgent>().As<IAWCTCustomizedFormAgent>().InstancePerDependency();

            
        }
        public int Order
        {
            get { return 1; }
        }
    }
}