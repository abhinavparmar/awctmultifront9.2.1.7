﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AWCTFormContactUsViewModel : BaseViewModel
    {
      public string   BillingAddress1     {get;set;}
      public string   BillingAddress2     {get;set;}
      public string   City                {get;set;}
      public string   StateProvince       {get;set;}
      public string   Country             {get;set;}
      public string   ZipCode             {get;set;}
      public string ResidentialCommercial { get; set; }
      public List<SelectListItem> States { get; set; }
      public List<SelectListItem> Countries { get; set; }

    }
}
