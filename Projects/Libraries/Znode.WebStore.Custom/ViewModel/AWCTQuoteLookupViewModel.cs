﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AWCTQuoteLookupViewModel : BaseViewModel
    {
        public string QuoteNumber { get; set; }
        public string QuoteEmailAddress { get; set; }  
        public string Custom1        { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
    }
}
