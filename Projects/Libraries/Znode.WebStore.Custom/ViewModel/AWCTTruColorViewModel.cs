﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AWCTTruColorViewModel : BaseViewModel
    {
        public List<AWCTTruColor> TruColorList { get; set; }
    }


    public class AWCTTruColor
    {

        public string GlobalAttributeName { get; set; }
        public string GlobalAttributeLabel { get; set; }
        public string TruColorValue { get; set; }
        public string ColorHashCode { get; set; }
        public string TruColorName { get; set; }
        public string TruColorOptionName { get; set; }
        public string SvgFilePath { get; set; }
    }
}
