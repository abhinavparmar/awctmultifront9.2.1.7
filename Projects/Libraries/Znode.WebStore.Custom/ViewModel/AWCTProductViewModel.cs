﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AWCTProductViewModel : ProductViewModel
    {
        public List<ProductDetailGridViewModel> ColorSizeStockList { get; set; }

        public string ConfigurableProductName { get; set; }
    }

    public class ProductDetailGridViewModel
    {
        public int AssociatedProductId { get; set; }
        public string AssociatedProductSKU { get; set; }
        public decimal Stock { get; set; }
        public int MinQty { get; set; }
        public int MaxQty { get; set; }
        public string OutOfstockOptions { get; set; }
        public decimal? ReOrderLevel { get; set; }

        public string Size { get; set; }

        public string Color { get; set; }

        public string SwatchImage { get; set; }

        public string ProductImage { get; set; }

        public string ZoomImage { get; set; }

        public decimal SalesPrice { get; set; }

        public string ColorNumber { get; set; }
        public string CatalogName { get; set; }
        public string EstimateQuantitySymbol { get; set; }
        public string DisableClearanceQuantity { get; set; }

        public string IsDutiable { get; set; }
        public int NeedDays { get; set; }

        public string starMarkingWithQty { get; set; }
    }
}
