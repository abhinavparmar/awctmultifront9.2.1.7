﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore;
using Znode.WebStore;

namespace Znode.WebStore.Custom.ViewModel
{
    public class AWCTBecomeAContributerViewModel : BaseViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string SchooName { get; set; }
        public string StateProvince { get; set; }
        public string ZipCode { get; set; }
        public string AlternateNumber { get; set; }
        public string CellPhone { get; set; }
        public string EmailAddress { get; set; }
        public string CityName { get; set; }
        public string Website { get; set; }
        public string BlogContributerAppl { get; set; }
        public string DanceHistory { get; set; }
        public string UniquePrespective { get; set; }
        public string YourExpert { get; set; }
        public string BlogPost { get; set; }
        public string FavoriteThing { get; set; }
        public string SocialMediaScale { get; set; }
        public string Relevant { get; set; }
        public string AdditionalDetails { get; set; }

    }
}
