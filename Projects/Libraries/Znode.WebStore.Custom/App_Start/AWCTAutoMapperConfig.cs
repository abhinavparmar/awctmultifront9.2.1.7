﻿using AutoMapper;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomizedFormModel;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom
{
    public static class AWCTAutoMapperConfig
    {
        public static void Execute()
        {
            Mapper.CreateMap<AssociatedProductDetails, ConfigurableAssociatedProductDetails>().ReverseMap();
            Mapper.CreateMap<PriceSizeGroup, ConfigurablePriceSizeGroup>().ReverseMap();
            Mapper.CreateMap<ConfigurableProductModel, ConfigurableProductViewModel>().ReverseMap();
            Mapper.CreateMap<ProductDetailGridViewModel, ProductDetailGridModel>().ReverseMap();
            Mapper.CreateMap<AWCTProductViewModel, AWCTPublishProductModel>().ReverseMap();
            Mapper.CreateMap<AWCTTruColorViewModel, AWCTTruColorModel>().ReverseMap();
            Mapper.CreateMap<AWCTTruColor, AWCTTruColorList>().ReverseMap();
            Mapper.CreateMap<ShoppingCartModel, AddToCartViewModel>();
            Mapper.CreateMap<AWCTNewCustomerApplicationModel, AWCTNewCustomerApplicationViewModel>().ReverseMap();
            Mapper.CreateMap<AWCTBecomeAContributerModel, AWCTBecomeAContributerViewModel>().ReverseMap();
            Mapper.CreateMap<AWCTModelSearchModel, AWCTModelSearchViewModel>().ReverseMap();
            /*Start Quote Lookup*/
            Mapper.CreateMap<AWCTQuoteLookupModel, AWCTQuoteLookupViewModel>().ReverseMap();
            /*End Quote Lookup*/
        }
    }
}
