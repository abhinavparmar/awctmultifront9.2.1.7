﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;

namespace Znode.WebStore.Custom.Agents.IAgents
{
    public interface IAWCTCartAgent: ICartAgent
    {
        CartViewModel RemoveDiscount(string Promostatus);
        CartViewModel CalculateShippingCartReview(int shippingOptionId, int shippingAddressId, string shippingCode, string additionalInstruction = "");
    }

}
