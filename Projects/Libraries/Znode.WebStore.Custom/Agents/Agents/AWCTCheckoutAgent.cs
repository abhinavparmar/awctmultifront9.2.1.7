﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.Models;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTCheckoutAgent : CheckoutAgent
    {
        private readonly ICartAgent _cartAgent;
        private readonly IUserClient _userClient;
        private readonly IPaymentAgent _paymentAgent;

        public AWCTCheckoutAgent(IShippingClient shippingsClient, IPaymentClient paymentClient, IPortalProfileClient profileClient, ICustomerClient customerClient, IUserClient userClient, IOrderClient orderClient, IAccountClient accountClient, IWebStoreUserClient webStoreAccountClient, IPortalClient portalClient, IShoppingCartClient shoppingCartClient, IAddressClient addressClient) : base(shippingsClient, paymentClient, profileClient, customerClient, userClient, orderClient, accountClient, webStoreAccountClient, portalClient, shoppingCartClient, addressClient)
        {
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _userClient = GetClient<IUserClient>(userClient);
            _paymentAgent = new PaymentAgent(GetClient<PaymentClient>(), GetClient<OrderClient>());

        }

        public override OrdersViewModel SubmitOrder(SubmitOrderViewModel submitOrderViewModel)
        {
           
            //Get cart from session or by cookie.
            ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                      _cartAgent.GetCartFromCookie();
            string discount = cartModel.Custom3;
            try
            {
                cartModel.Custom1 = ((string[])submitOrderViewModel.Custom1)[0];
                cartModel.Custom2 = ((string[])submitOrderViewModel.Custom2)[0];
                cartModel.Custom3 = ((string[])submitOrderViewModel.Custom3)[0];
                cartModel.CSRDiscountDescription = discount;               
            }
            catch (Exception)
            {
                cartModel.Custom1 = null;
                ZnodeLogging.LogMessage("SubmitOrder .Web service Date Null", ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            }

            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);

            OrdersViewModel modelorder = base.SubmitOrder(submitOrderViewModel);
            modelorder.Custom1 = cartModel.Custom1;
            modelorder.Custom4 = discount;
            return modelorder;
        }
        public override List<BaseDropDownOptions> PaymentOptions()
        {
            List<BaseDropDownOptions> list = base.PaymentOptions();
            list[1].Id = "_50";
            return list;
        }
        public override GatewayResponseModel GetPaymentResponse(ShoppingCartModel cartModel, SubmitOrderViewModel submitOrderViewModel)
        {

            // Map shopping Cart model and submit Payment view model to Submit payment model 

            SubmitPaymentModel model = PaymentViewModelMap.ToModel(cartModel, submitOrderViewModel);

            // Map Customer Payment Guid for Save Credit Card 
            if (!string.IsNullOrEmpty(submitOrderViewModel.CustomerGuid) && string.IsNullOrEmpty(cartModel.UserDetails.CustomerPaymentGUID))
            {
                UserModel userModel = _userClient.GetUserAccountData(submitOrderViewModel.UserId);
                userModel.CustomerPaymentGUID = submitOrderViewModel.CustomerGuid;
                _userClient.UpdateCustomerAccount(userModel);

                UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

                if (string.IsNullOrEmpty(userViewModel.CustomerPaymentGUID))
                {
                    userViewModel.CustomerPaymentGUID = submitOrderViewModel.CustomerGuid;
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                }
            }

            model.Total = _paymentAgent.GetOrderTotal();
            string[] data = submitOrderViewModel.PODocumentName.Split(',');
            if (data.Length > 0)
            {
                if (data[0] == "credit_card_50")
                {
                    model.Total = ConvertTotalToLocale((Convert.ToDecimal(cartModel.Total) / 2).ToString("#.##"));
                }
            }
            return _paymentAgent.ProcessPayNow(model);
        }
        #region Private Method
        //to convert total amount to locale wise
        private string ConvertTotalToLocale(string total)
            => total.Replace(",", ".");
        #endregion
    }
}
