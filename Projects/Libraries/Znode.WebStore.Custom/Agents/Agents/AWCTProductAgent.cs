﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.Sample.Api.Model;
using Znode.Sample.Api.Model.CustomProductModel;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;


namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTProductAgent : ProductAgent, IAWCTProductAgent
    {
        #region Private Variables
        private readonly ICustomerReviewClient _reviewClient;
        private readonly IAWCTPublishProductClient _productClient;
        private readonly IWebStoreProductClient _webstoreProductClient;
        private readonly ISearchClient _searchClient;
        private readonly IHighlightClient _highlightClient;
        private readonly IPublishCategoryClient _publishCategoryClient;

        #endregion

        #region Public Constructor
        public AWCTProductAgent(ICustomerReviewClient reviewClient, IAWCTPublishProductClient productClient, IWebStoreProductClient webstoreProductClient, ISearchClient searchClient, IHighlightClient highlightClient, IPublishCategoryClient publishCategoryClient) :
            base(reviewClient, productClient, webstoreProductClient, searchClient, highlightClient, publishCategoryClient)
        {
            _productClient = GetClient<IAWCTPublishProductClient>(productClient);
        }
        #endregion

        public ConfigurableProductModel GetConfigurableProductViewModel(int productID)
        {
            ConfigurableProductViewModel viewModel = _productClient.GetConfigurableProductViewModel(productID, GetRequiredFilters(), GetProductExpands());
            ConfigurableProductModel configurableViewModel = viewModel.ToViewModel<ConfigurableProductModel>();
            return configurableViewModel;
        }

        public ConfigurableProductModel GetPriceSizeList(int configurableProductId)
        {
            ConfigurableProductViewModel viewModel = _productClient.GetPriceSizeList(configurableProductId, GetRequiredFilters(), GetProductExpands());
            ConfigurableProductModel configurableViewModel = viewModel.ToViewModel<ConfigurableProductModel>();
            return configurableViewModel;
        }

        public AWCTTruColorViewModel GetGlobalAttributeData(string globalAttributeCodes)
        {
            AWCTTruColorModel truColorModel = _productClient.GetGlobalAttributeData(globalAttributeCodes, GetRequiredFilters(), GetProductExpands());
            AWCTTruColorViewModel viewModel = truColorModel.ToViewModel<AWCTTruColorViewModel>();
            return viewModel;
        }
       
        public new AWCTProductViewModel GetProduct(int productID)
        {
            if (productID > 0)
            {
                //set user id for profile base pricing.
                _productClient.UserId = (GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId).GetValueOrDefault();

                AWCTPublishProductModel model = _productClient.GetPublishProduct(productID, GetRequiredFilters(), GetProductExpands());
                if (HelperUtility.IsNotNull(model))
                {
                    AWCTProductViewModel viewModel = model.ToViewModel<AWCTProductViewModel>();

                    string minQuantity = viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity);

                    decimal quantity = Convert.ToDecimal(string.IsNullOrEmpty(minQuantity) ? "0" : minQuantity);

                    viewModel.IsCallForPricing = false;//Convert.ToBoolean(viewModel.Attributes?.Value(ZnodeConstant.CallForPricing)) || (model.Promotions?.Any(x => x.PromotionType?.Replace(" ", "") == ZnodeConstant.CallForPricing)).GetValueOrDefault();

                    viewModel.ProductType = viewModel.Attributes.CodeFromSelectValue(ZnodeConstant.ProductType);

                    //Check Main Product inventory
                    //CheckInventory(viewModel, quantity);

                    //Get Addon SKu from required addons.
                    string addonSKu = string.Join(",", viewModel.AddOns?.Where(x => x.IsRequired)?.Select(y => y.AddOnValues?.FirstOrDefault(x => x.IsDefault)?.SKU));

                    // if ((!string.IsNullOrEmpty(addonSKu) && (HelperUtility.IsNotNull(viewModel.Quantity) && viewModel.Quantity > 0)) || (!string.IsNullOrEmpty(addonSKu) && (Equals(viewModel.ProductType, ZnodeConstant.GroupedProduct))))
                    //Check Associated addon inventory.
                    //CheckAddOnInvenTory(viewModel, addonSKu, quantity);

                    if (!viewModel.IsCallForPricing)
                        GetProductFinalPrice(viewModel, viewModel.AddOns, quantity, addonSKu);

                    viewModel.ParentProductId = productID;
                    viewModel.IsConfigurable = HelperUtility.IsNotNull(viewModel.Attributes?.Find(x => x.ConfigurableAttribute?.Count > 0));

                    //AddToRecentlyViewProduct(viewModel.ConfigurableProductId > 0 ? viewModel.ConfigurableProductId : viewModel.PublishProductId);

                    Znode.Engine.WebStore.Helper.SetProductCartParameter(viewModel);

                    if (viewModel.IsConfigurable)
                        GetConfigurableValues(model, viewModel);

                    return viewModel;
                }
            }
            throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorProductNotFound);
        }

        //This method only returns the brief details of a published product from Mongo.
        //public new AWCTProductViewModel GetExtendedProductDetails(int productID, string[] expandKeys)
        //{
        //    if (productID > 0 && HelperUtility.IsNotNull(expandKeys) && expandKeys.Length > 0)
        //    {
        //        //set user id for profile base pricing.
        //        _productClient.UserId = (GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey)?.UserId).GetValueOrDefault();

        //        _productClient.SetPublishStateExplicitly(PortalAgent.CurrentPortal.PublishState);
        //        _productClient.SetLocaleExplicitly(PortalAgent.CurrentPortal.LocaleId);
        //        _productClient.SetDomainHeaderExplicitly(GetCurrentWebstoreDomain());

        //        AWCTPublishProductModel model = _productClient.GetExtendedPublishProductDetails(productID, GetRequiredFilters(), GetProductExpands(false, expandKeys));
        //        if (HelperUtility.IsNotNull(model))
        //        {
        //            AWCTProductViewModel viewModel = model.ToViewModel<AWCTProductViewModel>();

        //            string minQuantity = viewModel.Attributes?.Value(ZnodeConstant.MinimumQuantity);

        //            decimal quantity = Convert.ToDecimal(string.IsNullOrEmpty(minQuantity) ? "0" : minQuantity);

        //            viewModel.IsCallForPricing = Convert.ToBoolean(viewModel.Attributes?.Value(ZnodeConstant.CallForPricing)) || (model.Promotions?.Any(x => x.PromotionType?.Replace(" ", "") == ZnodeConstant.CallForPricing)).GetValueOrDefault();

        //            viewModel.MiscellaneousDetails.ProductType = viewModel.Attributes.CodeFromSelectValue(ZnodeConstant.ProductType);

        //            ////Check Main Product inventory
        //            //CheckInventory(viewModel, quantity);

        //            ////Get Addon SKu from required addons.
        //            string addonSKu = string.Join(",", viewModel.AddOns?.Where(x => x.IsRequired)?.Select(y => y.AddOnValues?.FirstOrDefault(x => x.IsDefault)?.SKU));

        //            //if ((!string.IsNullOrEmpty(addonSKu) && (HelperUtility.IsNotNull(viewModel.InventoryDetails.Quantity) && viewModel.InventoryDetails.Quantity > 0)) || (!string.IsNullOrEmpty(addonSKu) && (Equals(viewModel.MiscellaneousDetails.ProductType, ZnodeConstant.GroupedProduct))))
        //            //    //Check Associated addon inventory.
        //            //    CheckAddOnInventory(viewModel, addonSKu, quantity);

        //            //if (!viewModel.IsCallForPricing)
        //            //    GetProductFinalPrice(viewModel, viewModel.AddOns, quantity, addonSKu);

        //            viewModel.ParentProductId = productID;
        //            viewModel.IsConfigurable = HelperUtility.IsNotNull(viewModel.Attributes?.Find(x => x.ConfigurableAttribute?.Count > 0));

        //            //AddToRecentlyViewProduct(viewModel.MiscellaneousDetails.ConfigurableProductId > 0 ? viewModel.MiscellaneousDetails.ConfigurableProductId : viewModel.PublishProductId);

        //            //Engine.WebStore.Helper.SetProductCartParameter(viewModel);
        //            //Uncomment this
        //            SetProductCartParameter(viewModel);

        //            if (viewModel.IsConfigurable)
        //                GetConfigurableValues(model, viewModel);

        //            //check if the product is added in wishlist for the logged in user. If so, binds its wishlistId
        //            //  BindProductWishListDetails(viewModel);

        //            return viewModel;
        //        }
        //    }
        //    throw new ZnodeException(ErrorCodes.NotFound, WebStore_Resources.ErrorProductNotFound);
        //}

        #region Private Methods
        public void SetProductCartParameter(ShortProductViewModel product)
        {
            if (HelperUtility.IsNotNull(product))
            {
                string sku = !string.IsNullOrEmpty(product.ConfigurableProductSKU) ? product.SKU : product.Attributes.Value(ZnodeConstant.ProductSKU);
                product.CartParameter = new Dictionary<string, string>();
                // product.MiscellaneousDetails.ProductType = product.Attributes.SelectAttributeList(ZnodeConstant.ProductType)?.FirstOrDefault()?.Code;
                product.CartParameter.Add("ProductId", product.PublishProductId.ToString());
                product.CartParameter.Add("SKU", sku);
                // product.CartParameter.Add("ProductType", product.MiscellaneousDetails.ProductType);
                product.CartParameter.Add("Quantity", product.Attributes.Value(ZnodeConstant.MinimumQuantity));
                product.CartParameter.Add("ParentProductId", product.ParentProductId.ToString());
                product.CartParameter.Add("ConfigurableProductSKUs", product.ConfigurableProductSKU);
                product.CartParameter.Add("AddOnProductSKUs", string.Empty);
                product.CartParameter.Add("PersonalisedCodes", string.Empty);
                product.CartParameter.Add("PersonalisedValues", string.Empty);
                if (product.AddOns.Count > 0)
                    product.CartParameter.Add("AutoAddonSKUs", string.Join(",", product.AddOns.Where(x => x.IsAutoAddon)?.Select(y => y.AutoAddonSKUs)));
                //switch (product.MiscellaneousDetails.ProductType)
                //{
                //    case ZnodeConstant.GroupedProduct:
                //        product.CartParameter.Add("GroupProductSKUs", string.Empty);
                //        product.CartParameter.Add("GroupProductsQuantity", string.Empty);
                //        break;
                //    case ZnodeConstant.BundleProduct:
                //        product.CartParameter.Add("BundleProductSKUs", string.Empty);
                //        break;
                //    default:
                //        break;
                //}

            }
        }
        private void GetConfigurableValues(PublishProductModel model, AWCTProductViewModel viewModel)
        {
            viewModel.ConfigurableData = new ConfigurableAttributeViewModel();
            //Select Is Configurable Attributes list

            List<AttributesViewModel> ConfigurableAttributes = viewModel.ConfigurableData.ConfigurableAttributes;
            ZnodeLogging.LogMessage("ConfigurableAttributes" + ConfigurableAttributes, ZnodeLogging.Components.OMS.ToString(), System.Diagnostics.TraceLevel.Info);
            if (ConfigurableAttributes == null)
                viewModel.ConfigurableData.ConfigurableAttributes = new List<AttributesViewModel>();
           
            viewModel.ConfigurableData.ConfigurableAttributes = viewModel?.Attributes.Where(x => x.IsConfigurable && x.ConfigurableAttribute?.Count > 0)?.ToList();
            //Assign select attribute values.
            viewModel.ConfigurableData.ConfigurableAttributes.ForEach(x => x.SelectedAttributeValue = new[] { x.ConfigurableAttribute?.FirstOrDefault()?.AttributeValue });
        }

        private void AddToRecentlyViewProduct(int productId)
        {
            if (productId > 0)
            {
                List<string> productIds = GetFromSession<List<string>>(ZnodeConstant.RecentlyViewProducts);
                if (!IsProductExistInList(productIds, Convert.ToString(productId)))
                    SetMaxRecentProductInSession(Convert.ToString(productId));
            }
        }

        private bool IsProductExistInList(List<string> productIds, string productId)
        {
            if (productIds?.Count > 0)
                return productIds.Contains(productId) ? true : false;

            return false;
        }

        private void SetMaxRecentProductInSession(string productId)
        {
            //List of product ids from cookies
            List<string> productIds = GetFromSession<List<string>>(ZnodeConstant.RecentlyViewProducts);

            if (HelperUtility.IsNull(productIds))
                productIds = new List<string>();

            productIds.Add(productId);

            int maxItemToDisplay = /*MvcDemoConstants.MaxRecentViewItemToDisplay*/15;

            //If exceed of max limit of recently view product remove last product from list.
            if (productIds.Count > maxItemToDisplay)
                for (int count = 0; count < productIds.Count - maxItemToDisplay; count++)
                    productIds.RemoveAt(0);

            if (productIds.Count > 0)
                SaveInSession(ZnodeConstant.RecentlyViewProducts, productIds);
        }
        private ExpandCollection GetProductExpands(bool isProductDetails = false)
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ExpandKeys.Promotions);
            expands.Add(ExpandKeys.Inventory);
            expands.Add(ExpandKeys.ProductReviews);
            expands.Add(ExpandKeys.Pricing);
            expands.Add(ExpandKeys.ProductTemplate);
            expands.Add(ExpandKeys.AddOns);
            expands.Add(ExpandKeys.SEO);
            expands.Add(ExpandKeys.Brand);
            if (isProductDetails)
                expands.Add(ExpandKeys.ConfigurableAttribute);
            return expands;
        }
        #endregion
        //Get Product Details For Review.
        public override ProductReviewViewModel GetProductForReview(int productID, string productName, decimal? rating)
        {
            _productClient.SetProfileIdExplicitly(Helper.GetProfileId().GetValueOrDefault());
            PublishProductModel model = _productClient.GetPublishProduct(productID, GetRequiredFilters(), new ExpandCollection { ExpandKeys.SEO });
            if (HelperUtility.IsNotNull(model))
            {
                ProductReviewViewModel viewModel = model.ToViewModel<ProductReviewViewModel>();
                viewModel.PublishProductId = model.ConfigurableProductId > 0 ? model.ConfigurableProductId : model.PublishProductId;
                return viewModel;
            }
            else
                return new ProductReviewViewModel { PublishProductId = productID, ProductName = productName };
        }
    }
}
