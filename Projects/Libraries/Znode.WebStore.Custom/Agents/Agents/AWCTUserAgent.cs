﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class AWCTUserAgent : UserAgent
    {
        #region Private Variables
        private readonly ICountryClient _countryClient;
        private readonly IWebStoreUserClient _webStoreAccountClient;
        private readonly IWishListClient _wishListClient;
        private readonly IUserClient _userClient;
        private readonly IPublishProductClient _productClient;
        private readonly ICustomerReviewClient _customerReviewClient;
        private readonly IOrderClient _orderClient;
        private readonly IGiftCardClient _giftCardClient;
        private readonly IAccountClient _accountClient;
        private readonly IAccountQuoteClient _accountQuoteClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IPortalCountryClient _portalCountryClient;
        private readonly IProductAgent _productAgent;
        private readonly IShippingClient _shippingClient;
        private readonly ICartAgent _cartAgent;
        private readonly IPaymentClient _paymentClient;
        private readonly ICustomerClient _customerClient;
        private readonly IRoleClient _roleClient;
        private readonly IStateClient _stateClient;
       // private readonly IAWCTUserClient _AWCTUserClient;
        private string _domainName;
        private string _domainKey;


        #endregion
        public AWCTUserAgent
            (ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient, IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient, orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _countryClient = GetClient<ICountryClient>(countryClient);
            _webStoreAccountClient = GetClient<IWebStoreUserClient>(webStoreAccountClient);
            _wishListClient = GetClient<IWishListClient>(wishListClient);
            _userClient = GetClient<IUserClient>(userClient);           
            _productClient = GetClient<IPublishProductClient>(productClient);
            _customerReviewClient = GetClient<ICustomerReviewClient>(customerReviewClient);
            _orderClient = GetClient<IOrderClient>(orderClient);
            _giftCardClient = GetClient<IGiftCardClient>(giftCardClient);
            _accountClient = GetClient<IAccountClient>(accountClient);
            _accountQuoteClient = GetClient<IAccountQuoteClient>(accountQuoteClient);
            _orderStateClient = GetClient<IOrderStateClient>(orderStateClient);
            _portalCountryClient = GetClient<IPortalCountryClient>(portalCountryClient);
            _productAgent = new ProductAgent(GetClient<CustomerReviewClient>(), GetClient<PublishProductClient>(), GetClient<WebStoreProductClient>(), GetClient<SearchClient>(), GetClient<HighlightClient>(), GetClient<PublishCategoryClient>());
            _shippingClient = GetClient<IShippingClient>(shippingClient);
            _cartAgent = new CartAgent(GetClient<ShoppingCartClient>(), GetClient<PublishProductClient>(), GetClient<AccountQuoteClient>(), GetClient<UserClient>());
            _paymentClient = GetClient<IPaymentClient>(paymentClient);
            _customerClient = GetClient<ICustomerClient>(customerClient);
            _roleClient = GetClient<RoleClient>();
            _stateClient = GetClient<IStateClient>(stateClient);
        }
        private string SetTrackingUrl(string trackingNo, int shippingId)
        {
            string trackingUrl = GetTrackingUrlByShippingId(shippingId);
            return string.IsNullOrEmpty(trackingUrl) ? trackingNo : "<a target=_blank href=" + GetTrackingUrlByShippingId(shippingId) + trackingNo + ">" + trackingNo + "</a>";
        }
        private string GetTrackingUrlByShippingId(int shippingId)
            => _shippingClient.GetShipping(shippingId)?.TrackingUrl;
        //Get order details to generate the order reciept.
        public override OrdersViewModel GetOrderDetails(int orderId, int portalId = 0)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            filters.Add("IsHistoryUserReceipt", FilterOperators.Equals, "1");
            UserViewModel userViewModel = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey);

            if (HelperUtility.IsNotNull(userViewModel) && orderId > 0)
            {
                try
                {
                    OrderModel orderModel = _orderClient.GetOrderById(orderId, SetExpandsForReceipt(), filters);
                    orderModel.BillingAddressHtml = orderModel.BillingAddress.DisplayName + "</Br>" + orderModel.BillingAddress.Address1 + "</Br>" + orderModel.BillingAddress.Address2 +
                                                   "</Br>" + orderModel.BillingAddress.CityName + "</Br>" + orderModel.BillingAddress.StateName + "</Br>" + orderModel.BillingAddress.PostalCode;
                    orderModel.OrderLineItems[0].ShippingAddressHtml = orderModel.ShippingAddress.DisplayName + "</Br>" + orderModel.ShippingAddress.Address1 + "</Br>" + orderModel.ShippingAddress.Address2 +
                                                   "</Br>" + orderModel.ShippingAddress.CityName + "</Br>" + orderModel.ShippingAddress.StateName + "</Br>" + orderModel.ShippingAddress.PostalCode;
                    OrdersViewModel orderDetails = orderModel?.ToViewModel<OrdersViewModel>();
                    orderDetails.CultureCode = PortalAgent.CurrentPortal?.CultureCode;

                    return orderDetails;
                }
                catch
                {
                    return base.GetOrderDetails(orderId, portalId);
                }
            }
            return new OrdersViewModel();
        }

        private static ExpandCollection SetExpandsForReceipt()
        {
            ExpandCollection expands = new ExpandCollection();
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderLineItems.ToString());
            expands.Add(ExpandKeys.Store);
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentType.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodePaymentSetting.ToString());
            expands.Add(ZnodeOmsOrderDetailEnum.ZnodeOmsOrderState.ToString());
            expands.Add(ExpandKeys.ZnodeShipping);
            expands.Add(ExpandKeys.IsFromOrderReceipt);
            expands.Add(ExpandKeys.PortalTrackingPixel);
            expands.Add(ExpandKeys.IsWebStoreOrderReciept);
            return expands;
        }

        public override LoginViewModel Login(LoginViewModel model)
        {
            LoginViewModel loginViewModel;
            try
            {
                model.PortalId = PortalAgent.CurrentPortal?.PortalId;
                //Authenticate the user credentials.
                UserModel userModel = _userClient.Login(UserViewModelMap.ToLoginModel(model), GetExpands());
                if (HelperUtility.IsNotNull(userModel))
                {
                    //Check of Reset password Condition.
                    if (HelperUtility.IsNotNull(userModel.User) && !string.IsNullOrEmpty(userModel.User.PasswordToken))
                    {
                        loginViewModel = UserViewModelMap.ToLoginViewModel(userModel);
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword, true, loginViewModel);
                    }
                    // Check user associated profiles.                
                    if (!userModel.Profiles.Any() && !userModel.IsAdminUser)
                        return ReturnErrorModel(WebStore_Resources.ProfileLoginFailedErrorMessage);

                    SetLoginUserProfile(userModel);
                    UserViewModel userViewModel = userModel.ToViewModel<UserViewModel>();
                    //bind userName to userViewModel
                    userViewModel.UserName = userModel.UserName;
                    //Save the User Details in Session.
                    SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                    SaveInSession("LoginFirstName", userModel.FirstName);
                    return UserViewModelMap.ToLoginViewModel(userModel);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorModel(ex.ErrorMessage, true);
                    case ErrorCodes.AccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorAccountLocked);
                    case ErrorCodes.LoginFailed:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                    case ErrorCodes.TwoAttemptsToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorTwoAttemptsRemain);
                    case ErrorCodes.OneAttemptToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorOneAttemptRemain);
                    case ErrorCodes.IsUsed:
                        return ReturnErrorModel(WebStore_Resources.ErrorMultipleRecordsLogin);
                    default:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
            }
            return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
        }

        //Set the Error properties values for Login View Model.
        private LoginViewModel ReturnErrorModel(string errorMessage, bool hasResetPassword = false, LoginViewModel model = null)
        {
            if (HelperUtility.IsNull(model))
                model = new LoginViewModel();

            //Set Model Properties.
            model.HasError = true;
            model.IsResetPassword = hasResetPassword;
            model.ErrorMessage = string.IsNullOrEmpty(errorMessage) ? WebStore_Resources.InvalidUserNamePassword : errorMessage;
            return model;
        }
        //Forgot Password, use to send the Reset Password Link to the user.
        public override UserViewModel ForgotPassword(UserViewModel model)
        {
            if (HelperUtility.IsNotNull(model))
            {
                try
                {
                    //trim all white spaces from username
                    model.UserName = Regex.Replace(model.UserName, @"\s", "");
                    //Get the User Details based on username.
                    UserModel userModel = _userClient.GetAccountByUser(model.UserName);
                    if (HelperUtility.IsNotNull(userModel))
                    {
                        if (!model.HasError)
                        {
                            //Check if the Username Validates or not.
                            if (string.Equals(model.UserName, userModel.UserName, StringComparison.InvariantCultureIgnoreCase) || string.Equals(model.UserName, userModel.Email, StringComparison.InvariantCultureIgnoreCase))
                            {
                                //Set the Current Domain Url.
                                model.BaseUrl = GetDomainUrl();

                                //Reset the Password for the user, to send the Reset Email Password Link.
                                model = ResetPassword(model);
                                return model;
                            }
                            else
                                return SetErrorProperties(model, WebStore_Resources.ValidEmailAddress);
                        }
                    }
                    else
                        return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                }
                catch (ZnodeException ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                    switch (ex.ErrorCode)
                    {
                        case ErrorCodes.IsUsed:
                            return SetErrorProperties(model, WebStore_Resources.ErrorMultipleRecordsResetPassword);
                        default:
                            return SetErrorProperties(model, WebStore_Resources.ErrorInvalidUsernamePassword);

                    }
                }
                catch (Exception ex)
                {
                    ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                    return SetErrorProperties(model, WebStore_Resources.InvalidAccountInformation);
                }
            }

            return model;
        }

       
        //Update the Model with error properties.Return the updated model in UserViewModelFormat.
        private UserViewModel SetErrorProperties(UserViewModel model, string errorMessage)
        {
            model.HasError = true;
            model.ErrorMessage = errorMessage;
            return model;
        }
        //Method gets the Domain Base Url.
        private string GetDomainUrl()
            => (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)))
            ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;

        //Reset the Password for the user.
        private UserViewModel ResetPassword(UserViewModel model)
        {
            try
            {
                _userClient.ForgotPassword(UserViewModelMap.ToUserModel(model));
                model.SuccessMessage = WebStore_Resources.SuccessResetPassword;
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                SetErrorProperties(model, ex.ErrorMessage);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
            }
            return model;
        }
        // Returns the Expands needed for the user agent.       
        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.GiftCardHistory,
                };
        }

    }
}
