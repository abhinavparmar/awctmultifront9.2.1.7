﻿
using System.Web.Mvc;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Controllers;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Core.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Controllers
{
    public class AWCTCartController : CartController
    {
        #region Private Variables
        private readonly IAWCTCartAgent _cartAgent;
        private readonly IWSPromotionAgent _promotionAgent;
        private readonly string shoppingCartView = "Cart";
        //private readonly string shoppingCart = "_shoppingCart";
        #endregion
        public AWCTCartController(IAWCTCartAgent cartAgent, IPortalAgent portalAgent, IWSPromotionAgent promotionAgent) : base(cartAgent, portalAgent, promotionAgent)
        {
            _cartAgent = cartAgent;
            _promotionAgent = promotionAgent;
        }
        [Authorize]
        public override ActionResult Index()
        {          
            CartViewModel cartdata = _cartAgent.GetCart(false);          
            cartdata.ShippingModel = _promotionAgent.GetPromotionListByPortalId(PortalAgent.CurrentPortal.PortalId);            
            return View(shoppingCartView, cartdata);
        }
        public override ActionResult GetShoppingCart()
        {           
            CartViewModel cartdata = _cartAgent.GetCart();
            cartdata.ShippingModel = _promotionAgent.GetPromotionListByPortalId(PortalAgent.CurrentPortal.PortalId);
            return View(shoppingCartView, cartdata);
        }
       
    }
}
