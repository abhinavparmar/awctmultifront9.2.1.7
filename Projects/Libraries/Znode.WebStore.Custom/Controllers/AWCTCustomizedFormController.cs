﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.WebStore.Controllers;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.Controllers
{
    public class AWCTCustomizedFormController : BaseController
    {
        private readonly IAWCTCustomizedFormAgent _AWCTCustomizedFormAgent;

        public AWCTCustomizedFormController(IAWCTCustomizedFormAgent AWCTCustomizedFormAgent)
        {
            _AWCTCustomizedFormAgent = AWCTCustomizedFormAgent;
        }


        //Get : Create CaseRequest for customer feedback form.
        [HttpGet]
        public virtual ActionResult NewCustomerApplication()
        {

            return View("../AWCTCustomizedForms/NewCustomerApplication");
        }

        [HttpPost]
        public virtual ActionResult NewCustomerApplication(AWCTNewCustomerApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isMailSend = _AWCTCustomizedFormAgent.NewCustomerApplication(model);
                if (isMailSend == true)
                {
                    return View("../AWCTCustomizedForms/AWCTCustomizedSuccessForm");
                }
                else
                {
                    return Json(new
                    {
                        Type = false ? "success" : "error",
                        Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                Type = false ? "success" : "error",
                Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
            }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public virtual ActionResult NewCustomerApplicationSuccess()
        {
            return View("../AWCTCustomizedForms/NewCustomerApplicationSuccess");
        }



        [HttpGet]
        public virtual ActionResult ModelSearch()
        {
            return View("../AWCTCustomizedForms/ModelSearch");
        }

        [HttpPost]
        public virtual ActionResult ModelSearch(AWCTModelSearchViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isMailSend = _AWCTCustomizedFormAgent.ModelSearch(model);
                if (isMailSend == true)
                {
                    return View("../AWCTCustomizedForms/AWCTCustomizedSuccessForm");
                }
                else
                {
                    return Json(new
                    {
                        Type = false ? "success" : "error",
                        Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                Type = false ? "success" : "error",
                Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
            }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public virtual ActionResult ModelSearchSuccess()
        {
            return View("../AWCTCustomizedForms/ModelSearchSuccess");
        }
        [HttpGet]
        public virtual ActionResult PreviewShow()
        {
            return View("../AWCTCustomizedForms/PreviewShow");
        }

        [HttpPost]
        public virtual ActionResult PreviewShow(AWCTPreviewShowViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isMailSend = _AWCTCustomizedFormAgent.PreviewShow(model);
                if (isMailSend == true)
                {
                    return View("../AWCTCustomizedForms/AWCTCustomizedSuccessForm");
                }
                else
                {
                    return Json(new
                    {
                        Type = false ? "success" : "error",
                        Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                Type = false ? "success" : "error",
                Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
            }, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public virtual ActionResult PreviewShowSuccess()
        {
            return View("../AWCTCustomizedForms/PreviewShowSuccess");
        }
        [HttpGet]
        public virtual ActionResult BecomeAContributer()
        {
            return View("../AWCTCustomizedForms/BecomeAContributer");
        }

        [HttpPost]
        public virtual ActionResult BecomeAContributer(AWCTBecomeAContributerViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isMailSend = _AWCTCustomizedFormAgent.BecomeAContributer(model);
                if (isMailSend == true)
                {
                    return View("../AWCTCustomizedForms/AWCTCustomizedSuccessForm");
                }
                else
                {
                    return Json(new
                    {
                        Type = false ? "success" : "error",
                        Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                Type = false ? "success" : "error",
                Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
            }, JsonRequestBehavior.AllowGet);

        }

        /*Start Quote Lookup*/
        [HttpGet]
        public virtual ActionResult QuoteLookup()
        {
            return View("../AWCTCustomizedForms/QuoteLookup");
        }

        [HttpPost]
        public virtual ActionResult QuoteLookup(AWCTQuoteLookupViewModel model)
        {
            if (ModelState.IsValid)
            {
                bool isMailSend = _AWCTCustomizedFormAgent.QuoteLookup(model);
                if (isMailSend == true)
                {
                    return View("../AWCTCustomizedForms/AWCTCustomizedSuccessForm");
                }
                else
                {
                    return Json(new
                    {
                        Type = false ? "success" : "error",
                        Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new
            {
                Type = false ? "success" : "error",
                Message = false ? WebStore_Resources.SendMailMessage : WebStore_Resources.ErrorEmailSend
            }, JsonRequestBehavior.AllowGet);

        }
        /*End Quote Lookup*/
    }
}
