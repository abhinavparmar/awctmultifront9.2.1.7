﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Engine.Api.Client;
using Znode.Sample.Api.Model.CustomizedFormModel;

namespace Znode.Api.Client.Custom.Clients.IClients
{
    public interface IAWCTCustomizedFormClient
    {
        bool ModelSearch(AWCTModelSearchModel model);
        bool NewCustomerApplication(AWCTNewCustomerApplicationModel model);
        bool BecomeAContributer(AWCTBecomeAContributerModel model);
        bool PreviewShow(AWCTPreviewShowModel model);
        /*Start Quote Lookup*/
        bool QuoteLookup(AWCTQuoteLookupModel model);
        /*End Quote Lookup*/
    }
}
