﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Api.Client.Custom.Clients.IClients;
using Znode.Api.Model.Custom.CustomizedFormModel;
using Znode.Engine.Api.Client;
using Znode.Api.Client.Custom.Endpoints;
using Znode.Admin.Custom.Endpoints.Custom;
using Znode.Engine.Api.Models.Responses;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Exceptions;
using Znode.Api.Model.CustomizedFormModel;
using Znode.Sample.Api.Model.CustomizedFormModel;

namespace Znode.Api.Client.Custom.Clients
{
    public class AWCTCustomizedFormClient : BaseClient, IAWCTCustomizedFormClient
    {
        public virtual bool ModelSearch(AWCTModelSearchModel model)
        {
            //Get Endpoint.
             string endpoint = AWCTCustomizedFormEndpoint.ModelSearch();
            //Get response
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }

        public virtual bool NewCustomerApplication(AWCTNewCustomerApplicationModel model)
        {
            //Get Endpoint.
            string endpoint = AWCTCustomizedFormEndpoint.NewCustomerApplication();
            //Get response
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            //return true;
            return response.IsSuccess;
        }

        public virtual bool BecomeAContributer(AWCTBecomeAContributerModel model)
        {
            //Get Endpoint.
            string endpoint = AWCTCustomizedFormEndpoint.BecomeAContributer();
            //Get response
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            //return true;
            return response.IsSuccess;
        }

        public virtual bool PreviewShow(AWCTPreviewShowModel model)
        {
            //Get Endpoint.
            string endpoint = AWCTCustomizedFormEndpoint.PreviewShow();
            //Get response
            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            //return true;
            return response.IsSuccess;
        }
        /*Start Quote Lookup*/
        public virtual bool QuoteLookup(AWCTQuoteLookupModel model)
        {

            string endpoint = AWCTCustomizedFormEndpoint.QuoteLookup();

            ApiStatus status = new ApiStatus();
            TrueFalseResponse response = PostResourceToEndpoint<TrueFalseResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            Collection<HttpStatusCode> expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NoContent };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return response.IsSuccess;
        }
        /*End Quote Lookup*/
    }


}
