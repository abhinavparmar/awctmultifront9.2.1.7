﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;

namespace Znode.Api.Client.Custom.Endpoints
{
    public class AWCTPublishProductEndpoint : PublishProductEndpoint
    {
        public static string GetConfigurableProductViewModel(int publishProductId) => $"{ApiRoot}/AWCTPublishProduct/getconfigurableproductviewmodel/{publishProductId}";
        public static string GetPriceSizeList(int configurableProductId) => $"{ApiRoot}/AWCTPublishProduct/getpricesizelist/{configurableProductId}";

        public static string GetAWCTPublishProduct(int publishProductId) => $"{ApiRoot}/AWCTPublishProduct/get/{publishProductId}";

        public static string GetExtendedProductDetails(int publishProductId) => $"{ApiRoot}/AWCTPublishProduct/getextendedproductdetails/{publishProductId}";

        public static string GetGlobalAttributeData(string globalAttributeCodes) => $"{ApiRoot}/awctpublishproduct/getglobalattributedata/{globalAttributeCodes}";
        
    }
}
