﻿using Znode.Libraries.Observer;
using Znode.Libraries.Data.DataModel;

namespace Znode.Engine.Connector
{
    /// <summary>
    /// This class is use for adding terminals for connecting external application. 
    /// Using this class user can connect and insert data like external ERP, etc.
    /// User need to write specific terminal code and implemantation in it.
    /// </summary>
    public class GlobalConnector
    {
        #region Private Variables
        Connector<ZnodeOmsOrderDetail> myMessageToken;
        readonly EventAggregator eventAggregator;
        #endregion

        #region Constructor
        public GlobalConnector(EventAggregator eve)
        {
            eventAggregator = eve;
          var _res =  eve.Attach<ZnodeOmsOrderDetail>(this.SubmitCart);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// This is sample method to understand implemantation of connector.
        /// </summary>
        /// <param name="model"></param>
        private void SubmitCart(ZnodeOmsOrderDetail model)
        {

            //Submit Cart
            eventAggregator.Detach(myMessageToken);
        }
        #endregion
    }
}
