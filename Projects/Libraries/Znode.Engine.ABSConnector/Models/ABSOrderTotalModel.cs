﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.ABSConnector
{
    public class ABSOrderTotalModel: ABSRequestBaseModel
    {
        public string TotalOrderAmount { get; set; }
        public string TotalOrderUnits { get; set; }
        public string TotalOrderFreight { get; set; }
        public string TotalPromoDiscount { get; set; }
        public string TotalNbiDiscount { get; set; }
    }
}
