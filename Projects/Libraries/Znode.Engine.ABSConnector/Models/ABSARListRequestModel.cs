﻿namespace Znode.Engine.ABSConnector
{
    public class ABSARListRequestModel : ABSRequestBaseModel
    {
        public string Soldto { get; set; }
        public int NumberOfRecords { get; set; }
        public int CurrentRecord { get; set; }
        public string Shipto { get; set; }
    }
}
