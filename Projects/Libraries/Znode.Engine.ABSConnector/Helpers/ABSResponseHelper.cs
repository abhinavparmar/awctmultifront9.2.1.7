﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace Znode.Engine.ABSConnector
{
    public class ABSResponseHelper
    {
        //Get response models.
        public T GetResponseModel<T>(T responseModel, string requestType, string xmlResponse)
        {
            switch (requestType)
            {
                case ABSRequestTypes.ARList:
                    return (T)Convert.ChangeType(GetARListModel(responseModel as List<ABSARListResponseModel>, xmlResponse), typeof(T));
                case ABSRequestTypes.ARPayment:
                    return (T)Convert.ChangeType(GetARPaymentModel(responseModel as List<ABSARPaymentResponseModel>, xmlResponse), typeof(T));
                case ABSRequestTypes.Inventory:
                    return (T)Convert.ChangeType(GetInventoryModel(responseModel as List<ABSInventoryResponseModel>, xmlResponse), typeof(T));
                case ABSRequestTypes.OrderNumber:
                    return (T)Convert.ChangeType(GetOrderNumberModel(responseModel as List<ABSOrderNumberResponseModel>, xmlResponse), typeof(T));
                case ABSRequestTypes.OrderHistory:
                    return (T)Convert.ChangeType(GetOrderHistoryModel(responseModel as List<ABSOrderResponseDetailsModel>, xmlResponse), typeof(T));
                case ABSRequestTypes.OrderHistoryList:
                    return (T)Convert.ChangeType(GetTopOrderModel(responseModel as List<ABSOrderResponseTop5Model>, xmlResponse), typeof(T));
                case ABSRequestTypes.PDFPrint:
                    return (T)Convert.ChangeType(GetPDFPrintModel(responseModel as List<ABSPrintInfoResponseModel>, xmlResponse), typeof(T));
            }

            return responseModel;
        }

        //Map the ABS AR List Response Model.
        private List<ABSARListResponseModel> GetARListModel(List<ABSARListResponseModel> model, string xmlResponse)
        {
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var doc = XDocument.Parse(xmlResponse);

                return (from r in doc.Root.Elements("ResponseInfo")
                        select new ABSARListResponseModel()
                        {
                            ItemStatus = (string)r.Element("ItemStatus"),
                            ItemReferenceNumber = (string)r.Element("ItemReferenceNumber"),
                            ItemReferenceDate = (string)r.Element("ItemReferenceDate"),
                            ItemReferenceType = (string)r.Element("ItemReferenceType"),
                            ItemShipDate = (string)r.Element("ItemShipDate"),
                            ItemAmount = (string)r.Element("ItemAmount"),
                            ItemPoNumber = (string)r.Element("ItemPoNumber"),
                            ItemFreightAmount = (string)r.Element("ItemFreightAmount"),
                            ItemAdjustmentAmount = (string)r.Element("ItemAdjustmentAmount"),
                            ItemShipToNumber = (string)r.Element("ItemShipToNumber"),

                        }).ToList();
            }

            return new List<ABSARListResponseModel>();
        }

        //Map the ABS AR Payment Response Model.
        private List<ABSARPaymentResponseModel> GetARPaymentModel(List<ABSARPaymentResponseModel> model, string xmlResponse)
        {
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var doc = XDocument.Parse(xmlResponse);

                return (from r in doc.Root.Elements("ArPaymentResponse")
                        select new ABSARPaymentResponseModel()
                        {
                            ProcessSucessfully = (string)r.Element("ProcessSucessfully"),
                        }).ToList();
            }

            return new List<ABSARPaymentResponseModel>();
        }

        //Map the ABS Inventory Response Model.
        private List<ABSInventoryResponseModel> GetInventoryModel(List<ABSInventoryResponseModel> model, string xmlResponse)
        {
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var doc = XDocument.Parse(xmlResponse);

                return (from r in doc.Root.Elements("InventoryRecord")
                        select new ABSInventoryResponseModel()
                        {
                            UpcNumber = (string)r.Element("UpcNumber"),
                            InventoryCode = (string)r.Element("InventoryCode"),
                            InventoryQty = Convert.ToInt32((string)r.Element("InventoryQty")),
                            InventoryCutNumber = (string)r.Element("InventoryCutNumber"),
                            InventoryExpectedDate = (!Equals((string)r.Element("InventoryExpectedDate"), null)) ? DateTime.ParseExact((string)r.Element("InventoryExpectedDate"), "yyyyMMdd", CultureInfo.InvariantCulture) : (DateTime?)null,
                            InventroyCloseOut = (string)r.Element("InventroyCloseOut"),
                            SoldOutFlag = (string)r.Element("SoldOutFlag"),
                            SoldOutCode = (string)r.Element("SoldOutCode"),
                            SoldOutDescription = (string)r.Element("SoldOutDescription"),
                            SoldOutDate = (!Equals((string)r.Element("SoldOutDate"), null)) ? DateTime.ParseExact((string)r.Element("SoldOutDate"), "yyyyMMdd", CultureInfo.InvariantCulture) : (DateTime?)null,
                        }).ToList();
            }

            return new List<ABSInventoryResponseModel>();
        }

        //Map the ABS Order Number Response Model.
        private List<ABSOrderNumberResponseModel> GetOrderNumberModel(List<ABSOrderNumberResponseModel> model, string xmlResponse)
        {
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var doc = XDocument.Parse(xmlResponse);

                return (from r in doc.Root.Elements("ResponseInfo")
                        select new ABSOrderNumberResponseModel()
                        {
                            NextOrderNumber = (string)r.Element("NextOrderNumber"),
                        }).ToList();
            }

            return new List<ABSOrderNumberResponseModel>();
        }

        //Map the ABS Order History Response Model.
        private List<ABSOrderResponseDetailsModel> GetOrderHistoryModel(List<ABSOrderResponseDetailsModel> model, string xmlResponse)
        {
            List<ABSOrderResponseDetailsModel> obj = new List<Engine.ABSConnector.ABSOrderResponseDetailsModel>();
            ABSOrderResponseDetailsModel OrderModel = new Engine.ABSConnector.ABSOrderResponseDetailsModel();
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var test = xmlResponse.Replace("&", "&amp;");
                var doc = XDocument.Parse(test);

                OrderModel.SoldToInformation = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("SoldToInformation")
                                                select new ABSSoldToInformationModel()
                                                {
                                                    SoldToAddress1 = (string)r.Element("SoldToAddress1"),
                                                    SoldToAddress2 = (string)r.Element("SoldToAddress2"),
                                                    SoldToCity = (string)r.Element("SoldToCity"),
                                                    SoldToName = (string)r.Element("SoldToName"),
                                                    SoldToState = (string)r.Element("SoldToState"),
                                                    SoldToZip = (string)r.Element("SoldToZip"),
                                                }).FirstOrDefault();

                OrderModel.ShipToInformation = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("ShipToInformation")
                                                select new ABSShipToInformationModel()
                                                {
                                                    ShipToName = (string)r.Element("ShipToName"),
                                                    ShipToAddress1 = (string)r.Element("ShipToAddress1"),
                                                    ShipToAddress2 = (string)r.Element("ShipToAddress2"),
                                                    ShipToCity = (string)r.Element("ShipToCity"),
                                                    ShipToState = (string)r.Element("ShipToState"),
                                                    ShipToZip = (string)r.Element("ShipToZip"),
                                                }).FirstOrDefault();
                OrderModel.PaymentInformation = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("PaymentInformation")
                                                 select new ABSARPaymentRequestModel()
                                                 {
                                                     OpenTermsDescription = (string)r.Element("OpenTermsDescription")
                                                 }).FirstOrDefault();
                OrderModel.TrackingInformation = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("TrackingInformation")
                                                  select new ABSTrackingInformationModel()
                                                  {
                                                      TrackingNumber1 = (string)r.Element("TrackingNumber1"),
                                                      TrackingNumber2 = (string)r.Element("TrackingNumber2"),
                                                      TrackingNumber3 = (string)r.Element("TrackingNumber3"),
                                                      TrackingNumber4 = (string)r.Element("TrackingNumber4"),
                                                  }).FirstOrDefault();
                OrderModel.BillToInformation = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("BillToInformation")
                                                select new ABSBillToChangeRequestModel()
                                                {
                                                    BillToName = (string)r.Element("BillToName"),
                                                    BillToAddress1 = (string)r.Element("BillToAddress1"),
                                                    BillToAddress2 = (string)r.Element("BillToAddress2"),
                                                    BillToCity = (string)r.Element("BillToCity"),
                                                    BillToState = (string)r.Element("BillToState"),
                                                    BillToZip = (string)r.Element("BillToZip"),
                                                }).FirstOrDefault();
                OrderModel.OrderDetailLine = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("OrderDetailLine")
                                              select new ABSOrderDetailLineModel()
                                              {
                                                  LineStyleNumber = (string)r.Element("LineStyleNumber"),
                                                  LineStyleDescription = (string)r.Element("LineStyleDescription"),
                                                  LineColorDescription = (string)r.Element("LineColorDescription"),
                                                  LineColorCode = (string)r.Element("LineColorCode"),
                                                  LineUpcCode = (string)r.Element("LineUpcCode"),
                                                  LineSize = (string)r.Element("LineSize"),
                                                  LineQuantity = (string)r.Element("LineQuantity"),
                                                  LinePrice = (string)r.Element("LinePrice"),
                                                  LineItemStatus = (string)r.Element("LineStatus")
                                              }).ToList();
                OrderModel.OrderHeader = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation")
                                          select new ABSOrderHeaderModel()
                                          {
                                              OrderNumber = (string)r.Element("OrderNumber"),
                                              OrderDate = (string)r.Element("OrderDate"),
                                              OrderHeaderStatus = ((string)r.Element("OrderHeaderStatus"))
                                          }).FirstOrDefault();
                OrderModel.OrderTotal = (from r in doc.Elements("OrderInfoResponse").Elements("OrderInformation").Elements("OrderTotals")
                                         select new ABSOrderTotalModel()
                                         {
                                             TotalOrderAmount = (string)r.Element("TotalOrderAmount"),
                                             //TotalOrderUnits = (string)r.Element("TotalOrderUnits"),
                                             //TotalOrderFreight = (string)r.Element("TotalOrderFreight"),
                                             //TotalPromoDiscount = (string)r.Element("TotalPromoDiscount"),
                                             //TotalNbiDiscount = (string)r.Element("TotalNbiDiscount")
                                         }).FirstOrDefault();


            }
            obj.Add(OrderModel);
            return obj;
        }
        private List<ABSOrderResponseTop5Model> GetTopOrderModel(List<ABSOrderResponseTop5Model> model, string xmlResponse)
        {
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var test = xmlResponse.Replace("&", "&amp;");
                var doc = XDocument.Parse(test);

                var demo = (from r in doc.Elements("Top5SummaryInfo").Elements("Top5DetailInfo")
                            select new ABSOrderResponseTop5Model()
                            {
                                OrderDate = (string)r.Element("OrderDate"),
                                OrderNumber = (string)r.Element("OrderNumber"),
                                SummaryAmount = (string)r.Element("SummaryAmount"),
                                CustomerOrderNumber = (string)r.Element("CustomerOrderNumber"),
                                SummaryUnits = (string)r.Element("SummaryUnits"),
                            }).ToList();
                return demo;

            }

            return new List<ABSOrderResponseTop5Model>();
        }
        //Map the ABS Print Info Response Model.
        private List<ABSPrintInfoResponseModel> GetPDFPrintModel(List<ABSPrintInfoResponseModel> model, string xmlResponse)
        {
            if (!string.IsNullOrEmpty(xmlResponse))
            {
                var doc = XDocument.Parse(xmlResponse);

                return (from r in doc.Root.Elements("ResponseInfo")
                        select new ABSPrintInfoResponseModel()
                        {
                            DocumentURL = (string)r.Element("DocumentURL"),
                        }).ToList();
            }

            return new List<ABSPrintInfoResponseModel>();
        }
    }
}
