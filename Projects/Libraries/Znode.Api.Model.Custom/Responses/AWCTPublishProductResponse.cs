﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models.Responses;
using Znode.Sample.Api.Model.CustomProductModel;

namespace Znode.Sample.Api.Model.Responses
{
   public class AWCTPublishProductResponse : PublishProductResponse
    {
        public ConfigurableProductViewModel ConfigurableProductViewModel { get; set; }

        public AWCTPublishProductModel AWCTPublishProduct { get; set; }

        public AWCTTruColorModel AWCTTruColorModel { get; set; }
    }
}
