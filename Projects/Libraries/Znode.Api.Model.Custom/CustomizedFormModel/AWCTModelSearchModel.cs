﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Api.Model.Custom.CustomizedFormModel
{
    public class AWCTModelSearchModel : BaseModel
    {
        public int LocaleId { get; set; }
        public int CatalogId { get; set; }
        public int PortalId { get; set; }
        public string ModelName { get; set; }
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string StateProvince { get; set; }
        public string ZipCode { get; set; }
        public string DayPhone { get; set; }
        public string EveningPhone { get; set; }
        public string CellPhone { get; set; }
        public string EmailAddress { get; set; }
        public string ArmLength { get; set; }
        public string ChestMeasurement { get; set; }
        public string GirthMeasurement { get; set; }
        public string HipMeasurement { get; set; }
        public string InseamMeasurement { get; set; }
        public string WaistMeasurement { get; set; }
        public string Age { get; set; }
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string LeotardSize { get; set; }
        public string ShoeSize { get; set; }
        public string DanceStudioYouAttend { get; set; }
        public string AdditionalPhotoUpload { get; set; }
        public string HeadShotPhotoUpload { get; set; }
        public string FullLengthPhotoUpload { get; set; }
        public string AdditionalDetails { get; set; }
    }
}
