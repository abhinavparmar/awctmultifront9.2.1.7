﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Sample.Api.Model
{
  public class AWCTTruColorModel : BaseModel
    {
        public List<AWCTTruColorList> TruColorList { get; set; }
    }
    public class AWCTTruColorList
    {

        public string GlobalAttributeName { get; set; }
        public string GlobalAttributeLabel { get; set; }
        public string TruColorValue { get; set; }
        public string ColorHashCode { get; set; }
        public string TruColorName { get; set; }

        public string TruColorOptionName { get; set; }

    }


}
