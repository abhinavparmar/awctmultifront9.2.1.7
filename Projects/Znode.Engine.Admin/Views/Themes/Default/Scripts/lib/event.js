﻿

// Nav Drop List Building
var global = {
    // BINDINGS
    bind: {
        ResetTopCategory: function () {
            var _contenerWidth = $("#navbar-collapse-nav").css("width");
            var _menuWidth = 0;
            var html = document.createElement("div");

            $("#navbar-collapse-nav .dropdown").each(function () {
                _menuWidth += parseInt($(this).css("width").replace("px", ""));

                if (parseInt(_menuWidth) > (parseInt(_contenerWidth.replace("px", "")) - 50)) {
                    $(html).append($(this))
                }
                if (parseInt(_menuWidth) < parseInt(_contenerWidth.replace("px", ""))) {
                    $("#navbar-collapse-nav .navbar-nav").append($("#others-menu li:eq(0)"))
                }
            });

            $("#others-menu").append($(html));
        }
    }    
}

$(window).on("load",function () {
    //Product List widget Slider.
    $(".Product_List_Widget").owlCarousel({
        autoPlay: false,
        navigation: true,
        items: 4,
        itemsCustom: [
          [0, 1],
          [320, 1],
          [480, 2],
          [600, 2],
          [768, 3],
          [992, 3],
          [1200, 4]
        ],
        navigationText: [
          "<i class='glyphicon glyphicon-chevron-left'></i>",
          "<i class='glyphicon glyphicon-chevron-right'></i>"
        ]
    });

    // DatePicker
    $('.datepicker').datepicker();

    global.bind.ResetTopCategory();
    if ($("#others-menu .dropdown").length === 0) {
        $(".nav-drop-icon").hide();
    }
    else {
        $(".nav-drop-icon").show();
    }
    // Nav Drop Button
    $(".control_next").on("click", function () {
        $("#others-menu").toggle();
    });
});
$(window).bind('orientationchange', function (event) {
    if ($("#others-menu .dropdown").length === 0) {
        $(".nav-drop-icon").hide();
    }
    else {
        $(".nav-drop-icon").show();
    }
});
