﻿class ProductFeed extends ZnodeBase {
    _Model: any;
    _endPoint: Endpoint;
    _multiFastItemdata: Array<string>;

    constructor() {
        super();
        ProductFeed.prototype._multiFastItemdata = new Array<string>();
    }

    Init(): any {
        ProductFeed.prototype.DisplayXMLSiteMapType();
        ProductFeed.prototype.SetFeedIsSelectAllPortalOnInit();       
    }

    DisplayCustomDate(control): any  {
        if ($.trim($(control).val()) === "Use date / time of this update") {
            $('#CustomDate-content').show();
        }
        else {
            $('#CustomDate-content').hide();
        }

        if ($.trim($(control).val()) === "Use the database update date") {
            var dt = new Date(Date.now());
            var date = dt.toLocaleDateString() + " " + dt.toLocaleTimeString();
            $('#DBDate').val(date);
        }
    }

    DisplayXMLSiteMapType(): any {
        if ($('#ddlXMLSiteMap').val() == 'XmlSiteMap') {
            $('#rdbXMLSiteMapType').show();
            $('#GoogleFeedFields').hide();
        } else if ($('#ddlXMLSiteMap').val() == 'Google' || $('#ddlXMLSiteMap').val() == 'Bing') {
            $('#rdbXMLSiteMapType').hide();
            $('#GoogleFeedFields').show();
        } 
    }

    ShowHideStoreList(ctrl): any {
        if (ctrl != '') {
            if (ctrl.checked) {
                $(".chkStoresList").hide();
                $("#StoreName-error").text('').removeClass("field-validation-error").hide();
                $("#txtPortalName").parent("div").removeClass('input-validation-error');
            } else {
                ZnodeBase.prototype.SetInitialMultifastselectInput(null, null, $("#txtPortalName"));
                $(".chkStoresList").show();
            }
        }
    }

    public DeleteProductFeed(control): void {
        var productFeedId = DynamicGrid.prototype.GetMultipleSelectedIds();
        if (productFeedId.length > 0) {
            Endpoint.prototype.DeleteProductFeed(productFeedId, function (res) {
                DynamicGrid.prototype.RefreshGridOndelete(control, res);
            });
        }
    }

    public CheckPortalCheckBoxByIds(ids: string, lastModification: string): void {
        var arr = ids.split(',');
        for (var i = 0; i < arr.length; i++) {
            $('input:checkbox[name="PortalId"][value=' + arr[i] + ']').prop('checked', true);
            if (arr[i] == '0') {
                $(".chkStoresList").find("input[type=checkbox]").each(function () {
                    $(this).prop('checked', false);
                });
                $(".chkStoresList").hide();
            }
        }     

        if (lastModification === "Use date / time of this update") {
            $('#CustomDate-content').show();
        }
        else {
            $('#CustomDate-content').hide();
        }
    }

    SetPortals() {
        ProductFeed.prototype._multiFastItemdata = new Array<string>();
        if ($('input:checkbox[name="PortalId"]').prop('checked') == false) {
            $("#Stores").val();
            $(".chkStoresList").find(".fstChoiceItem").each(function () {
                if ($(this).data('value') != undefined)
                    ProductFeed.prototype._multiFastItemdata.push($(this).data('value'));
            });
            if (ProductFeed.prototype._multiFastItemdata.length == 0) {
                $("#StoreName-error").text('').text(ZnodeBase.prototype.getResourceByKeyName("ErrorSelectPortal")).addClass("field-validation-error").show();
                $("#txtPortalName").parent("div").addClass('input-validation-error');
                return false;
            }
            else {
                $("#StoreName-error").text('').removeClass("field-validation-error").hide();
                $("#txtPortalName").parent("div").removeClass('input-validation-error');
            }
        } else {
            ProductFeed.prototype._multiFastItemdata.push("0");
            $("#StoreName-error").text('').removeClass("field-validation-error").hide();
            $("#txtPortalName").parent("div").removeClass('input-validation-error');
        }
        $("#txtPortalName").val(ProductFeed.prototype._multiFastItemdata);
        $("#Stores").val(ProductFeed.prototype._multiFastItemdata);
        return true;
    }

    SetFeedIsSelectAllPortalOnInit() {        
        if ($('input:checkbox[name="PortalId"]').prop('checked') == true) {
            $(".chkStoresList").hide();
        } else {
            if (($('#Stores').val() != undefined) && ($('#Stores').val() != "")) {
                var portalsArray = $('#Stores').val().split(',');
                Endpoint.prototype.GetPortalList(Constant.storelist, function (response) {
                    ZnodeBase.prototype.SetInitialMultifastselectInput(portalsArray, response, $("#txtPortalName"));
                });
            }
            else {
                ZnodeBase.prototype.SetInitialMultifastselectInput(null, null, $("#txtPortalName"));
            }
            $(".chkStoresList").show();
        }
    }    

    public GenerateProductFeed(url): void {
        Endpoint.prototype.GenerateProductFeed(url, function (response) {
            if (response.success) {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "success", isFadeOut, fadeOutTime);
            }
            else {
                ZnodeNotification.prototype.DisplayNotificationMessagesHelper(response.message, "error", isFadeOut, fadeOutTime);
            }
        });
    }
}

$(document).off("click", "#ZnodeProductFeed .z-download");
$(document).on("click", "#ZnodeProductFeed .z-download", function (e) {
    e.preventDefault();
    ProductFeed.prototype.GenerateProductFeed($(this).attr('href'));
});